from torch import nn
from torchvision.models import mobilenet_v2 as _mobilenet_v2


class MobileNetV2(nn.Module):
    def __init__(self, pretrained=False):
        super(MobileNetV2, self).__init__()
        net = _mobilenet_v2(pretrained)
        features = net.features
        self.layer1 = nn.Sequential(*features[0:4])
        self.layer2 = nn.Sequential(*features[4:7])
        self.layer3 = nn.Sequential(*features[7:14])
        self.layer4 = nn.Sequential(*features[14:])

        self.layers_out_filters = [24, 32, 96, 1280]

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x2 = self.layer3(x)
        x3 = self.layer4(x2)
        return x, x2, x3


def mobilenet_v2(pretrained=False):
    return MobileNetV2(pretrained)


if __name__ == '__main__':
    import torch

    x = torch.rand(1, 3, 416, 416)
    net = mobilenet_v2(pretrained=True)
    out = net(x)
