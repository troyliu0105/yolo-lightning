from . import darknet
from . import mobilenet
from . import resnet

__all__ = ['backbone_fn']

backbone_fn = {
    'darknet21': darknet.darknet21,
    'darknet53': darknet.darknet53,
    'resnet18': resnet.resnet18,
    'resnet34': resnet.resnet34,
    'resnet50': resnet.resnet50,
    'resnet101': resnet.resnet101,
    'resnet151': resnet.resnet152,
    'resnext50_32x4d': resnet.resnext50_32x4d,
    'resnext101_32x8d': resnet.resnext101_32x8d,
    'wide_resnet50_2': resnet.wide_resnet50_2,
    'wide_resnet101_2': resnet.wide_resnet101_2,
    'mobilenet_v2': mobilenet.mobilenet_v2,
    # 'mobilenetv3_small': mobilenet_v3.mobilenetv3_small,
    # 'mobilenetv3_large': mobilenet_v3.mobilenetv3_large
}
