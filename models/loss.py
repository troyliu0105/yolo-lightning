from typing import Tuple, Optional

import torch
from torch import Tensor, nn

from utils import xywh2xyxy, bbox_iou, bbox_wh_iou
from utils.snippets import to_cpu_float

__all__ = ['YOLOLoss', 'FocalLoss', 'GIOULoss']


class YOLOLoss(nn.Module):
    # noinspection PyArgumentList
    def __init__(self, cfg_yolo):
        super(YOLOLoss, self).__init__()
        self.anchors = cfg_yolo['anchors']
        self.num_classes: int = cfg_yolo['classes']
        self.img_dim = (cfg_yolo['im_size'], cfg_yolo['im_size'])
        self.ignore_thresh = cfg_yolo['ignore_thresh']
        self.strides = cfg_yolo['strides']

        scales = cfg_yolo['loss_scales']
        self.scale_xy = scales['xy']
        self.scale_wh = scales['wh']
        self.scale_obj = scales['conf_obj']
        self.scale_noobj = scales['conf_noobj']
        self.scale_cls = scales['cls']
        self.bce_loss = nn.BCEWithLogitsLoss(reduction='mean')
        self.focal_loss = FocalLoss(nn.BCEWithLogitsLoss(), gamma=2, alpha=0.25, reduction='mean')
        self.ce_loss = nn.CrossEntropyLoss()
        self.mse_loss = nn.MSELoss(reduction='none')
        self.smooth_l1 = nn.SmoothL1Loss(reduction='none')

    def forward(self, feats: Tuple[Tensor], target=None):
        """
        compute output, losses, metrics

        :param feats:       feature maps list
        :param img_dim:     (img_h, img_w)
        :param target:      target tensor, shape: (nB, [nB_idx, label_idx, x, y, w, h]), coordinates are normalized
        :return:    outputs:    (nB, [x, y, w, h, conf, cls_pred])     <br>
                    losses:     List[loc, conf, clz]                    <br>
                    metrics:    Dict[str, float]

        """
        losses = []
        metrics = []
        outputs = []
        for feat, anchors, stride in zip(feats, self.anchors, self.strides):
            o, l, m = self._compute_single(feat, torch.tensor(anchors).float(), target, stride)
            losses.append(l)
            outputs.append(o)
            metrics.append(m)
        outputs = torch.cat(outputs, 1)
        losses = [sum(l) for l in zip(*losses)]
        if target is not None:
            metrics = {k: sum([item[k] for item in metrics]) for k in metrics[0]}
            avg_keys = ('metrics/recall50', 'metrics/recall75', 'metrics/precision',
                        'metrics/cls_acc', 'metrics/conf_obj', 'metrics/conf_noobj')
            for k in avg_keys:
                metrics[k] = metrics[k] / len(losses)
        return outputs, losses, metrics

    def _compute_single(self, ipt: Tensor, anchors: Tensor, targets=Optional[Tensor], stride: int = None):
        device, dtype = ipt.device, ipt.dtype
        nB = int(ipt.shape[0])
        grid_h = int(ipt.shape[2])
        grid_w = int(ipt.shape[3])
        num_anchors = anchors.size(0)

        # noinspection PyUnresolvedReferences
        prediction = ipt.view(nB, num_anchors, self.num_classes + 5, grid_h, grid_w).permute(0, 1, 3, 4, 2).contiguous()

        # get outputs
        x = torch.sigmoid(prediction[..., 0])
        y = torch.sigmoid(prediction[..., 1])
        w = prediction[..., 2]
        h = prediction[..., 3]
        pred_conf = prediction[..., 4]
        pred_cls = prediction[..., 5:]

        grid_x = torch.arange(grid_w).repeat(grid_h, 1).view(1, 1, grid_h, grid_w).to(device=device, dtype=dtype)
        grid_y = torch.arange(grid_h).repeat(grid_w, 1).t().view(1, 1, grid_h, grid_w).to(device=device, dtype=dtype)
        # noinspection PyArgumentList
        scaled_anchors = (anchors / stride).to(device=device, dtype=dtype)
        anchor_w = scaled_anchors[:, 0].view(1, num_anchors, 1, 1)
        anchor_h = scaled_anchors[:, 1].view(1, num_anchors, 1, 1)

        # noinspection PyArgumentList
        pred_boxes = torch.zeros_like(prediction[..., :4]).type_as(ipt)
        pred_boxes[..., 0] = x + grid_x
        pred_boxes[..., 1] = y + grid_y
        # TODO this is a kind of workaround to avoid w/h is to big, that leads to inf/nan
        pred_boxes[..., 2] = torch.exp(w) * anchor_w
        pred_boxes[..., 3] = torch.exp(h) * anchor_h

        output = torch.cat(
            (pred_boxes.view(nB, -1, 4) * stride,
             torch.sigmoid(pred_conf.view(nB, -1, 1)),
             torch.sigmoid(pred_cls.view(nB, -1, self.num_classes))),
            -1
        )
        if targets is None:
            return output.detach(), (0,), None
        else:
            (iou_scores, class_mask, obj_mask, noobj_mask, tx, ty, tw, th, tbox_scales, tcls, tconf) \
                = self.build_targets(
                pred_boxes,
                pred_cls,
                targets,
                scaled_anchors,
                self.ignore_thresh,
                stride)

            # Loss : Mask outputs to ignore non-existing objects (except with conf. loss)
            # tboxes = xywh2xyxy(tboxes[obj_mask])
            # pboxes = xywh2xyxy(pred_boxes[obj_mask])

            loss_xy = self.mse_loss(x[obj_mask], tx[obj_mask]) + self.mse_loss(y[obj_mask], ty[obj_mask])
            # loss_xy = self.smooth_l1(x[obj_mask], tx[obj_mask]) + self.smooth_l1(y[obj_mask], ty[obj_mask])
            loss_xy *= self.scale_xy
            loss_wh = self.mse_loss(w[obj_mask], tw[obj_mask]) + self.mse_loss(h[obj_mask], th[obj_mask])
            # loss_wh = self.smooth_l1(w[obj_mask], tw[obj_mask]) + self.smooth_l1(h[obj_mask], th[obj_mask])
            loss_wh *= self.scale_wh
            loss_loc = (loss_xy + loss_wh) * tbox_scales
            loss_loc = loss_loc.mean()
            loss_conf_obj = self.scale_obj * self.bce_loss(pred_conf[obj_mask], tconf[obj_mask])
            loss_conf_noobj = self.scale_noobj * self.bce_loss(pred_conf[noobj_mask], tconf[noobj_mask])
            # loss_conf_obj = self.scale_obj * self.focal_loss(pred_conf[obj_mask], tconf[obj_mask])
            # loss_conf_noobj = self.scale_noobj * self.focal_loss(pred_conf[noobj_mask], tconf[noobj_mask])
            loss_conf = loss_conf_obj + loss_conf_noobj
            loss_cls = self.scale_cls * self.bce_loss(pred_cls[obj_mask], tcls[obj_mask])
            # loss_cls = self.scale_cls * self.focal_loss(pred_cls[obj_mask], tcls[obj_mask])

            # Metrics
            pred_conf = torch.sigmoid(pred_conf)
            cls_acc = 100 * class_mask[obj_mask].mean()
            conf_obj = pred_conf[obj_mask].mean()
            conf_noobj = pred_conf[noobj_mask].mean()
            conf50 = (pred_conf > 0.5).type_as(ipt)
            iou50 = (iou_scores > 0.5).type_as(ipt)
            iou75 = (iou_scores > 0.75).type_as(ipt)
            detected_mask = conf50 * class_mask * tconf
            precision = torch.sum(iou50 * detected_mask) / (conf50.sum() + 1e-16)
            recall50 = torch.sum(iou50 * detected_mask) / (obj_mask.sum() + 1e-16)
            recall75 = torch.sum(iou75 * detected_mask) / (obj_mask.sum() + 1e-16)

            metrics = {
                "loss": to_cpu_float(loss_loc + loss_conf + loss_cls),
                "loc": to_cpu_float(loss_loc),
                "conf": to_cpu_float(loss_conf),
                "cls": to_cpu_float(loss_cls),
                "metrics/cls_acc": to_cpu_float(cls_acc),
                "metrics/recall50": to_cpu_float(recall50),
                "metrics/recall75": to_cpu_float(recall75),
                "metrics/precision": to_cpu_float(precision),
                "metrics/conf_obj": to_cpu_float(conf_obj),
                "metrics/conf_noobj": to_cpu_float(conf_noobj),
                "status/grid_size": float(grid_h),
            }

            return output.detach(), (loss_loc, loss_conf, loss_cls), metrics

    # noinspection PyArgumentList
    def build_targets(self, pred_boxes, pred_cls, target, anchors, ignore_threshold: float, stride) \
            -> Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor, Tensor]:
        """
        build targets tensor for training...

        :param pred_boxes: (N, anchors, grid_h, grid_w, 4)
        :param pred_cls: (N, anchors, grid_h, grid_w, num_classes)
        :param target: (Nb, (n, label, x, y, w, h))
        :param anchors: (nA, (aw, ah))
        :param ignore_threshold: iou to ignore training
        :return:
        """
        device, dtype = pred_boxes.device, pred_boxes.dtype

        nB = pred_boxes.size(0)
        nA = pred_boxes.size(1)
        nC = pred_cls.size(-1)
        nGh = pred_boxes.size(2)
        nGw = pred_boxes.size(3)

        # Output tensors
        obj_mask = torch.BoolTensor(nB, nA, nGh, nGw).fill_(0).to(device=device)
        noobj_mask = torch.BoolTensor(nB, nA, nGh, nGw).fill_(1).to(device=device)
        class_mask = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        iou_scores = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        # tboxes = torch.FloatTensor(nB, nA, nGh, nGw, 4).fill_(0).to(device=device, dtype=dtype)
        tx = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        ty = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        tw = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        th = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        tbox_scales = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)
        tcls = torch.FloatTensor(nB, nA, nGh, nGw, nC).fill_(0).to(device=device, dtype=dtype)
        tconf = torch.FloatTensor(nB, nA, nGh, nGw).fill_(0).to(device=device, dtype=dtype)

        # Convert to position relative to box
        # target_boxes = target[:, 2:6] * torch.tensor([nGw, nGh, nGw, nGh]).type_as(target)
        target_boxes = target[:, 2:6] / stride
        gxy = target_boxes[:, :2]
        gwh = target_boxes[:, 2:]
        # Get anchors with best iou
        ious = torch.stack([bbox_wh_iou(anchor, gwh) for anchor in anchors])
        best_ious, best_n = ious.max(0)
        # Separate target values
        b, target_labels = target[:, :2].long().t()
        gx, gy = gxy.t()
        gw, gh = gwh.t()
        gi, gj = gxy.long().t()
        # Set masks
        obj_mask[b, best_n, gj, gi] = True
        noobj_mask[b, best_n, gj, gi] = False

        # Set noobj mask to zero where iou exceeds ignore threshold
        # i:            sample num
        # anchor_ious:  ious between anchors and targets
        for i, anchor_ious in enumerate(ious.t()):
            noobj_mask[b[i], anchor_ious > ignore_threshold, gj[i], gi[i]] = False

        # tboxes[b, best_n, gj, gi, 0] = gx
        # tboxes[b, best_n, gj, gi, 1] = gy
        # tboxes[b, best_n, gj, gi, 2] = gw
        # tboxes[b, best_n, gj, gi, 3] = gh
        tx[b, best_n, gj, gi] = gx - gx.floor()
        ty[b, best_n, gj, gi] = gy - gy.floor()
        tw[b, best_n, gj, gi] = torch.log(gw / anchors[best_n][..., 0] + 1e-16)
        th[b, best_n, gj, gi] = torch.log(gh / anchors[best_n][..., 1] + 1e-16)
        tbox_scales[b, best_n, gj, gi] = (2 - (gw * gh / (nGw * nGh)))
        tbox_scales = tbox_scales[obj_mask]

        # One-hot encoding of label
        tcls[b, best_n, gj, gi, target_labels] = 1
        # Compute label correctness and iou at best anchor
        class_mask[b, best_n, gj, gi] = (pred_cls[b, best_n, gj, gi].argmax(-1) == target_labels).to(dtype=dtype)
        iou_scores[b, best_n, gj, gi] = bbox_iou(pred_boxes[b, best_n, gj, gi], target_boxes, x1y1x2y2=False)

        # set target conf to best iou
        tconf[b, best_n, gj, gi] = 1
        return iou_scores, class_mask, obj_mask, noobj_mask, tx, ty, tw, th, tbox_scales, tcls, tconf


class FocalLoss(nn.Module):
    # Wraps focal loss around existing loss_fcn() https://arxiv.org/pdf/1708.02002.pdf
    # i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=2.5)
    # Source: https://github.com/ultralytics/yolov3.git
    def __init__(self, loss_fcn, gamma=0.5, alpha=1.0, reduction='mean'):
        super(FocalLoss, self).__init__()
        loss_fcn.reduction = 'none'  # required to apply FL to each element
        self.loss_fcn = loss_fcn
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = reduction

    def forward(self, input, target):
        loss = self.loss_fcn(input, target)
        loss *= self.alpha * (1.000001 - torch.exp(-loss)) ** self.gamma  # non-zero power for gradient stability

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:  # 'none'
            return loss


class GIOULoss(nn.Module):
    def __init__(self, reduction='mean', from_xyxy=True):
        super(GIOULoss, self).__init__()
        self.reduction = reduction
        self.from_xxyy = from_xyxy

    def forward(self, b_p, b_g):
        assert b_p.shape == b_g.shape
        if not self.from_xxyy:
            b_p = xywh2xyxy(b_p)
            b_g = xywh2xyxy(b_g)
        x_p1 = torch.min(b_p[..., 0], b_p[..., 2])
        y_p1 = torch.min(b_p[..., 1], b_p[..., 3])
        x_p2 = torch.max(b_p[..., 0], b_p[..., 2])
        y_p2 = torch.max(b_p[..., 1], b_p[..., 3])

        area_b_g = (b_g[..., 2] - b_g[..., 0]) * (b_g[..., 3] - b_g[..., 1])
        area_b_p = (x_p2 - x_p1) * (y_p2 - y_p1)

        intersection = (torch.clamp_min(torch.min(x_p2, b_g[..., 2]), 0) -
                        torch.clamp_min(torch.max(x_p1, b_g[..., 0]), 0)) * \
                       (torch.clamp_min(torch.min(y_p2, b_g[..., 3]), 0) -
                        torch.clamp_min(torch.max(y_p1, b_g[..., 1]), 0))
        union = area_b_p + area_b_g - intersection

        area_c = (torch.max(x_p2, b_g[..., 2]) - torch.min(x_p1, b_g[..., 0])) * \
                 (torch.max(y_p2, b_g[..., 3]) - torch.min(y_p1, b_g[..., 1]))

        iou = intersection / union
        giou = iou - (area_c - union) / area_c
        giou = 1 - giou
        if self.reduction == 'mean':
            giou = torch.mean(giou)
        elif self.reduction == 'sum':
            giou = torch.sum(giou)
        elif self.reduction == 'none':
            pass
        return giou
