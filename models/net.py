import logging
from collections import OrderedDict

import torch
import torch.nn as nn

from models.backbone import backbone_fn
from utils.snippets import weights_init_normal

__all__ = ['YOLONet']


class YOLONet(nn.Module):
    __constants__ = ['embedding0', 'embedding1', 'embedding2']

    def __init__(self, cfg_yolo):
        super(YOLONet, self).__init__()
        # self.config = config
        # self.training = is_training
        # self.save_men = config['save_mem'] if defined(config, 'save_mem') else False
        model_params = cfg_yolo["model_params"]
        #  backbone
        _backbone_fn = backbone_fn[model_params["backbone"]]
        _backbone = _backbone_fn(model_params["backbone_pretrained"])
        p = model_params['backbone_pretrained']
        if isinstance(p, str) and p != '':
            params_dict = torch.load(p)
            self.backbone.load_state_dict(params_dict, strict=False)
            logging.info(f'Load pretrained backbone from {p}')
        elif isinstance(p, bool) and p:
            logging.info(f'Load pretrained backbone from official weights')
        _out_filters = _backbone.layers_out_filters
        #  embedding0
        final_out_filter_l = len(cfg_yolo["anchors"][-1]) * (5 + cfg_yolo["classes"])
        # self.embedding0 = self._make_embedding([512, 1024], _out_filters[-1], final_out_filter0)
        self.embedding_l = nn.Sequential(
            self._make_cbl(_out_filters[-1], 512, 1),
            self._make_cbl(512, 1024, 3),
            self._make_cbl(1024, 512, 1),
            self._make_cbl(512, 1024, 3),
            self._make_cbl(1024, 512, 1)
        )
        self.yolo_out_l = nn.Sequential(OrderedDict([
            ('yolo_conv_l', self._make_cbl(512, 1024, 3)),
            ('yolo_out_l', nn.Conv2d(1024, final_out_filter_l, kernel_size=1, stride=1, padding=0, bias=True))
        ]))
        #  embedding1
        final_out_filter_m = len(cfg_yolo["anchors"][-2]) * (5 + cfg_yolo["classes"])
        # self.embedding1_cbl = self._make_cbl(512, 256, 1)
        # self.embedding1_upsample = nn.Upsample(scale_factor=2, mode='nearest')
        # self.embedding1 = self._make_embedding([256, 512], _out_filters[-2] + 256, final_out_filter1)
        self.embedding_m_upsampler = nn.Sequential(
            self._make_cbl(512, 256, 1),
            nn.Upsample(scale_factor=2, mode='nearest')
        )
        self.embedding_m = nn.Sequential(
            self._make_cbl(_out_filters[-2] + 256, 256, 1),
            self._make_cbl(256, 512, 3),
            self._make_cbl(512, 256, 1),
            self._make_cbl(256, 512, 3),
            self._make_cbl(512, 256, 1)
        )
        self.yolo_out_m = nn.Sequential(OrderedDict([
            ('yolo_conv_m', self._make_cbl(256, 512, 3)),
            ('yolo_out_m', nn.Conv2d(512, final_out_filter_m, kernel_size=1, stride=1, padding=0, bias=True))
        ]))
        #  embedding2
        final_out_filter_s = len(cfg_yolo["anchors"][-3]) * (5 + cfg_yolo["classes"])
        # self.embedding2_cbl = self._make_cbl(256, 128, 1)
        # self.embedding2_upsample = nn.Upsample(scale_factor=2, mode='nearest')
        # self.embedding2 = self._make_embedding([128, 256], _out_filters[-3] + 128, final_out_filter2)
        self.embedding_s_upsampler = nn.Sequential(
            self._make_cbl(256, 128, 1),
            nn.Upsample(scale_factor=2, mode='nearest')
        )
        self.embedding_s = nn.Sequential(
            self._make_cbl(_out_filters[-3] + 128, 128, 1),
            self._make_cbl(128, 256, 3),
            self._make_cbl(256, 128, 1),
            self._make_cbl(128, 256, 3),
            self._make_cbl(256, 128, 1)
        )
        self.yolo_out_s = nn.Sequential(OrderedDict([
            ('yolo_conv_s', self._make_cbl(128, 256, 3)),
            ('yolo_out_s', nn.Conv2d(256, final_out_filter_s, kernel_size=1, stride=1, padding=0, bias=True))
        ]))
        self.apply(weights_init_normal)
        self.backbone = _backbone

    def _make_cbl(self, _in, _out, ks):
        """
        cbl = conv + batch_norm + leaky_relu
        :param _in:
        :param _out:
        :param ks:
        :return:
        """
        pad = (ks - 1) // 2 if ks else 0
        return nn.Sequential(OrderedDict([
            ("conv", nn.Conv2d(_in, _out, kernel_size=ks, stride=1, padding=pad, bias=False)),
            ("bn", nn.BatchNorm2d(_out)),
            ("relu", nn.LeakyReLU(0.1)),
        ]))

    def forward(self, x):
        #  backbone
        small, middle, large = self.backbone(x)
        #  yolo branch large (small feature mao)
        large = self.embedding_l(large)
        #  yolo branch middle
        middle = torch.cat([self.embedding_m_upsampler(large), middle], 1)
        middle = self.embedding_m(middle)
        #  yolo branch small
        small = torch.cat([self.embedding_s_upsampler(middle), small], 1)
        small = self.embedding_s(small)

        # compute yolo outputs
        small = self.yolo_out_s(small)
        middle = self.yolo_out_m(middle)
        large = self.yolo_out_l(large)

        return small, middle, large


if __name__ == "__main__":
    config = {"model_params": {"backbone_name": "darknet_53"}}
    m = YOLONet(config)
    x = torch.randn(1, 3, 416, 416)
    y0, y1, y2 = m(x)
    print(y0.size())
    print(y1.size())
    print(y2.size())
