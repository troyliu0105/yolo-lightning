import os
from collections import OrderedDict
from typing import Tuple, List

import numpy as np
import toml
import torch
import yaml
from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster.kmeans import kmeans, kmeans_visualizer
from pyclustering.utils.metric import type_metric, distance_metric
from torch import distributed, Tensor


def cluster_anchors(anchors: np.ndarray, nums: int, savefig=False) -> Tuple[np.ndarray, int]:
    # noinspection DuplicatedCode
    def bbox_wh_iou_cluster(wh1, wh2):
        w1, h1 = wh1[0], wh1[1]
        w2, h2 = wh2[0], wh2[1]
        inter_area = np.fmin(w1, w2) * np.fmin(h1, h2)
        union_area = (w1 * h1 + 1e-16) + w2 * h2 - inter_area
        return 1 - (inter_area / union_area).mean()

    metric = distance_metric(type_metric.USER_DEFINED, func=bbox_wh_iou_cluster, numpy_usage=True)
    initial_centers = kmeans_plusplus_initializer(anchors, nums).initialize()
    kmeans_instance = kmeans(anchors, initial_centers, metric=metric, ccore=True, itermax=500)
    kmeans_instance.process()
    cores = kmeans_instance.get_centers()
    if savefig:
        clusters = kmeans_instance.get_clusters()
        kmeans_visualizer.show_clusters(anchors, clusters, cores, initial_centers=initial_centers) \
            .savefig('cluster.png', dpi=400)
    dist = kmeans_instance.get_total_wce()
    cores = sorted(cores, key=lambda an: an[0] * an[1], reverse=False)
    cores = np.array(cores).astype('int').reshape((3, int(nums / 3), 2)).tolist()
    return cores, dist


def defined(config: dict, key: str) -> bool:
    return key in config


def is_nan(tensor: Tensor) -> bool:
    return bool(torch.isnan(tensor).any())


def is_distributed() -> bool:
    return distributed.is_available() and distributed.is_initialized()


def to_cpu(tensor: Tensor) -> Tensor:
    return tensor.detach().cpu()


def to_cpu_float(tensor: Tensor) -> float:
    return float(tensor.detach().cpu().item())


def load_classes(path: str) -> Tuple[List[str], List[tuple]]:
    """
    Loads class labels at 'path'
    """
    with open(os.path.expanduser(path), "r") as fp:
        names = fp.read().split("\n")[:-1]
    color_maps = [tuple(np.random.choice(255, 3, replace=False).tolist()) for _ in names]
    return names, color_maps


def load_cfg(cfg):
    ext = os.path.splitext(cfg)[1].lower()
    with open(cfg) as f:
        if ext == '.yaml':
            cfg = yaml.load(f, yaml.FullLoader)
        elif ext == '.toml':
            cfg = toml.load(f, _dict=OrderedDict)
        else:
            raise ValueError(f'cfg must be .yaml or .toml, current {ext}')
    return cfg


def dump_cfg(dir_path, cfg, ext='toml'):
    cfg_bak = f'{dir_path}/config.{ext}'
    with open(cfg_bak, 'w') as fp:
        if ext == 'yaml':
            yaml.dump(cfg, fp, default_flow_style=False, sort_keys=False)
        elif ext == 'toml':
            toml.dump(cfg, fp)
    return cfg_bak


def weights_init_normal(m: torch.nn.Module):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.03)
    elif classname.find("BatchNorm") != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.03)
        torch.nn.init.constant_(m.bias.data, 0.0)


def select_device(device='', apex=False):
    """
    Source: https://github.com/ultralytics/yolov3/
    :param device:
    :param apex:
    :return:
    """
    # device = 'cpu' or '0' or '0,1,2,3'
    cpu_request = device.lower() == 'cpu'
    if device and not cpu_request:  # if device requested other than 'cpu'
        os.environ['CUDA_VISIBLE_DEVICES'] = device  # set environment variable
        assert torch.cuda.is_available(), 'CUDA unavailable, invalid device %s requested' % device  # check availablity

    cuda = False if cpu_request else torch.cuda.is_available()
    if cuda:
        c = 1024 ** 2  # bytes to MB
        ng = torch.cuda.device_count()
        x = [torch.cuda.get_device_properties(i) for i in range(ng)]
        cuda_str = 'Using CUDA ' + ('Apex ' if apex else '')  # apex for mixed precision https://github.com/NVIDIA/apex
        for i in range(0, ng):
            if i == 1:
                cuda_str = ' ' * len(cuda_str)
            print("%sdevice%g _CudaDeviceProperties(name='%s', total_memory=%dMB)" %
                  (cuda_str, i, x[i].name, x[i].total_memory / c))
    else:
        print('Using CPU')

    print('')  # skip a line
    return torch.device('cuda:0' if cuda else 'cpu')
