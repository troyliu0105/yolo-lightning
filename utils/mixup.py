from typing import List, Tuple

import numpy as np
from PIL import Image

from utils import xywh2xyxy, xyxy2xywh

__all__ = ['mix_them_up']


def mix_them_up(dataset,
                idx: int,
                alpha_range: Tuple[float, float]):
    img, target = dataset.read_data_by_idx(idx)
    if np.random.randn() < 0.5:
        return img, target
    imgs = [dataset.read_data_by_idx(i) for i in np.random.choice(len(dataset), 1)]
    imgs.append((img, target))
    sizes = np.array([pair[0].size for pair in imgs])
    mixed_w = int(1.5 * sizes[:, 0].max())
    mixed_h = int(1.5 * sizes[:, 1].max())
    mixed = Image.new('RGBA', (mixed_w, mixed_h))
    # mixed.putalpha(0)
    targets = []
    last_alpha = -1
    for pic, target in imgs:
        boxes = xywh2xyxy(target[..., 1:])
        pic = pic.convert('RGBA')
        pic_w, pic_h = pic.size
        x = np.random.choice(mixed_w - pic_w)
        y = np.random.choice(mixed_h - pic_h)
        if last_alpha < 0:
            alpha = int(np.random.uniform(low=alpha_range[0], high=alpha_range[1]) * 255)
            last_alpha = alpha
        else:
            alpha = 255 - last_alpha
        pic.putalpha(alpha)
        mixed.paste(pic, (x, y), pic)
        boxes[..., [0, 2]] += x
        boxes[..., [1, 3]] += y
        target[..., 1:] = xyxy2xywh(boxes)
        targets.append(target)
    targets = np.vstack(targets)
    # mixed.show()
    return mixed, targets
