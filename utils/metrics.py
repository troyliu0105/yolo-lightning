from collections import namedtuple
from typing import List, Tuple, Union

import numpy as np
import torch
from torch import Tensor

import utils

__all__ = ['compute_ap_map']

BatchMetrics = namedtuple('BatchMetrics', ['tp', 'scores', 'labels'])


def batchs_statistics_flatten(statistics: List[BatchMetrics]):
    out = []
    for s in statistics:
        o = np.stack([s.tp, s.scores, s.labels]).T
        out.append(o)
    if len(out) > 0:
        out = np.concatenate(out)
    else:
        out = np.zeros((1, 3))
    return out[:, 0], out[:, 1], out[:, 2]


def ap_per_class(tp: np.ndarray,
                 conf: np.ndarray,
                 pred_cls: np.ndarray,
                 target_cls: np.ndarray) \
        -> Tuple[np.ndarray, Union[int, float, complex]]:
    """
    Compute the average precision, given the recall and precision curves.
    Source: https://github.com/rafaelpadilla/Object-Detection-Metrics.

    :param tp:          True positives (list).
    :param conf:        Objectness value from 0-1 (list).
    :param pred_cls:    Predicted object classes (list).
    :param target_cls:  True object classes (list).
    :return: (nClz, [unique_classes, ap, p, r, f1])
    """

    # Sort by objectness
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]

    # Find unique classes
    unique_classes = np.unique(target_cls)

    # Create Precision-Recall curve and compute AP for each class
    ap, p, r = [], [], []
    for c in unique_classes:
        i = pred_cls == c
        n_gt = (target_cls == c).sum()  # Number of ground truth objects
        n_p = i.sum()  # Number of predicted objects

        if n_p == 0 and n_gt == 0:
            continue
        elif n_p == 0 or n_gt == 0:
            ap.append(0)
            r.append(0)
            p.append(0)
        else:
            # Accumulate FPs and TPs
            fpc = (1 - tp[i]).cumsum()
            tpc = (tp[i]).cumsum()

            # Recall
            recall_curve = tpc / (n_gt + 1e-16)
            r.append(recall_curve[-1])

            # Precision
            precision_curve = tpc / (tpc + fpc)
            p.append(precision_curve[-1])

            # AP from recall-precision curve
            ap.append(compute_ap(precision_curve, recall_curve))

    # Compute F1 score (harmonic mean of precision and recall)
    p, r, ap = np.array(p), np.array(r), np.array(ap)
    f1 = 2 * p * r / (p + r + 1e-16)

    metrics = np.stack((unique_classes, ap, p, r, f1)).T
    return metrics, np.mean(metrics[:, 1]).item()


def compute_ap(precision: np.ndarray, recall: np.ndarray):
    """
    Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    :param precision:   The precision curve (list).
    :param recall:      The recall curve (list).
    :return:            The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.0], recall, [1.0]))
    mpre = np.concatenate(([0.0], precision, [0.0]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def get_batch_statistics(predicts: List[Tensor], targets: Tensor, iou_threshold: float) \
        -> List[BatchMetrics]:
    """
    Compute true positives, predicted scores and predicted labels per sample
    :param predicts:         preds processed by nms, shape (x1, y1, x2, y2, object_conf, class_score, class_pred)
    :param targets:         targets from dataset    shape (nB, [BNum, clz, x1, y1, x2, y2])
    :param iou_threshold:   tp threshold
    :return:                List[BatchMetrics]
    """
    batch_metrics = []
    for sample_i in range(len(predicts)):

        if predicts[sample_i] is None:
            continue

        output = predicts[sample_i]
        pred_boxes = output[:, :4]
        pred_scores = output[:, 4]
        pred_labels = output[:, -1]

        true_positives = torch.zeros(pred_boxes.shape[0])

        # 取得对应 bboxes
        annotations = targets[targets[:, 0] == sample_i][:, 1:]
        target_labels = annotations[:, 0] if annotations.size(0) else []
        if annotations.size(0):
            detected_boxes = []
            target_boxes = annotations[:, 1:]

            for pred_i, (pred_box, pred_label) in enumerate(zip(pred_boxes, pred_labels)):

                # If targets are found break
                if len(detected_boxes) == annotations.size(0):
                    break

                # Ignore if label is not one of the target labels
                if pred_label not in target_labels:
                    continue

                iou, box_index = utils.bbox_iou(pred_box.unsqueeze(0), target_boxes).max(0)
                if iou >= iou_threshold and box_index not in detected_boxes:
                    true_positives[pred_i] = 1
                    detected_boxes += [box_index]
        batch_metrics.append(BatchMetrics(true_positives.cpu().numpy(),
                                          pred_scores.cpu().numpy(),
                                          pred_labels.cpu().numpy()))
    return batch_metrics


def compute_ap_map(predicts: List[Tensor], labels: Tensor,
                   iou_threshold=0.3, from_xyxy=True):
    if not from_xyxy:
        labels[:, 2:] = utils.xywh2xyxy(labels[:, 2:])
    statistics = get_batch_statistics(predicts, labels, iou_threshold)
    tp, conf, clz = batchs_statistics_flatten(statistics)
    return ap_per_class(tp, conf, clz, labels[:, 1].cpu().numpy())
