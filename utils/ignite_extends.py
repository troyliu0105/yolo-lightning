import os
import tempfile

import torch
from ignite.contrib.handlers.tensorboard_logger import BaseOutputHandler, TensorboardLogger
from ignite.metrics import Metric

from utils import non_max_suppression, xywh2xyxy, \
    draw_boxes_on_images, make_grid, top_k_from_detections, rescale_boxes
from utils.augment_snippets import to_pic
from utils.metrics import compute_ap_map

__all__ = ['mAPMetric', 'ModelCheckpoint', 'ImageOutputHandler']


class mAPMetric(Metric):
    def __init__(self, iou_ignore=0.5, conf_threshold=0.5, nms_threshold=0.45, top_k=100, soft=False):
        super(mAPMetric, self).__init__(lambda output: (output['y_hat'].clone(), output['y'].clone()))
        self.iou_ignore = iou_ignore
        self.conf_threshold = conf_threshold
        self.nms_threshold = nms_threshold
        self.y_hat = []
        self.y = []
        self.top_k = top_k
        self.soft = soft

    def reset(self):
        self.y_hat = []
        self.y = []

    def update(self, output):
        assert len(output) == 2, f'Output must be tuple (y_hat, y), current: {len(output)}'
        y_hat, y = output
        detections = non_max_suppression(y_hat, self.conf_threshold, self.nms_threshold, soft=self.soft)
        detections = top_k_from_detections(detections, self.top_k)
        detections = [d.cpu() if d is not None else None for d in detections]
        # y[:, 2:] *= torch.tensor(img_size).repeat(2).type_as(y)
        y[:, 2:] = xywh2xyxy(y[:, 2:])
        y[:, 0] += self.y[-1][:, 0].max() + 1 if len(self.y) > 0 else 0
        self.y_hat += detections
        self.y.append(y)

    def compute(self):
        y_pred = self.y_hat
        y = torch.cat(self.y)
        statistics = compute_ap_map(y_pred, y, self.iou_ignore)
        return statistics


class ImageOutputHandler(BaseOutputHandler):
    def __init__(self, tag, class_map, color_map=None, conf_thres=0.5, nms_thres=0.45, soft=False, top_k=100):
        super(ImageOutputHandler, self). \
            __init__(tag,
                     output_transform=lambda opt: {'image': opt['x'],
                                                   'pred': opt['y_hat'],
                                                   'sizes': opt['sizes']})
        self.class_map = class_map
        self.color_map = color_map
        self.conf_thres = conf_thres
        self.nms_thres = nms_thres
        self.soft = soft
        self.top_k = top_k
        self.counts = 0

    def __call__(self, engine, logger, event_name):
        if not isinstance(logger, TensorboardLogger):
            raise RuntimeError("Handler 'OutputHandler' works only with TensorboardLogger")

        global_step = self.counts
        self.counts += 1
        metrics = self._setup_output_metrics(engine)
        image, pred, sizes = metrics['image'], metrics['pred'], metrics['sizes']
        img_dims = image.shape[2]
        images = [to_pic(t) for t in image]
        detections = non_max_suppression(pred, self.conf_thres, self.nms_thres, self.soft)
        detections = [[d[0].cpu(), d[1]] if d[0] is not None else [None, None] for d in zip(detections, sizes)]
        detections, sizes = list(zip(*detections))
        detections = top_k_from_detections(detections, self.top_k)
        # images, detections = self.rescale(images, detections, sizes, img_dims)
        imgs = draw_boxes_on_images(images, detections, self.class_map, self.color_map)
        imgs = make_grid(imgs)
        logger.writer.add_image(self.tag, imgs, global_step=global_step, dataformats='HWC')
        logger.writer.flush()


class ModelCheckpoint(object):
    def __init__(self, dirname, filename_prefix,
                 save_interval=None, score_name=None, n_saved=1,
                 atomic=True, require_empty=True,
                 create_dir=True,
                 save_as_state_dict=True):

        self._dirname = os.path.expanduser(dirname)
        self._fname_prefix = filename_prefix
        self._n_saved = n_saved
        self._save_interval = save_interval
        self._score_name = score_name
        self._atomic = atomic
        self._saved = []  # list of tuples (priority, saved_objects)
        self._iteration = 0
        self._save_as_state_dict = save_as_state_dict

        if create_dir:
            if not os.path.exists(dirname):
                os.makedirs(dirname)

        # Ensure that dirname exists
        if not os.path.exists(dirname):
            raise ValueError("Directory path '{}' is not found.".format(dirname))

        if require_empty:
            matched = [fname
                       for fname in os.listdir(dirname)
                       if fname.startswith(self._fname_prefix)]

            if len(matched) > 0:
                raise ValueError("Files prefixed with {} are already present "
                                 "in the directory {}. If you want to use this "
                                 "directory anyway, pass `require_empty=False`."
                                 "".format(filename_prefix, dirname))

    def _save(self, obj, path):
        if not self._atomic:
            self._internal_save(obj, path)
        else:
            tmp = tempfile.NamedTemporaryFile(delete=False, dir=self._dirname)
            try:
                self._internal_save(obj, tmp.file)
            except BaseException:
                tmp.close()
                os.remove(tmp.name)
                raise
            else:
                tmp.close()
                os.rename(tmp.name, path)

    def _internal_save(self, obj, path):
        if not self._save_as_state_dict:
            torch.save(obj, path)
        else:
            if not hasattr(obj, "state_dict") or not callable(obj.state_dict):
                raise ValueError("Object should have `state_dict` method.")
            torch.save(obj.state_dict(), path)

    def __call__(self, to_save, scores=None):
        if len(to_save) == 0:
            raise RuntimeError("No objects to checkpoint found.")

        self._iteration += 1

        if scores is not None:
            priority = scores

        else:
            priority = self._iteration
            if (self._iteration % self._save_interval) != 0:
                return

        if (len(self._saved) < self._n_saved) or (self._saved[0][0] < priority):
            saved_objs = []

            suffix = ""
            if self._score_name is not None:
                suffix = "_{}={:.7}".format(self._score_name, abs(priority))

            for name, obj in to_save.items():
                fname = '{}_{}_{}{}.pth'.format(self._fname_prefix, name, self._iteration, suffix)
                path = os.path.join(self._dirname, fname)

                self._save(obj=obj, path=path)
                saved_objs.append(path)

            self._saved.append((priority, saved_objs))
            self._saved.sort(key=lambda item: item[0])

        if len(self._saved) > self._n_saved:
            _, paths = self._saved.pop(0)
            for p in paths:
                os.remove(p)
