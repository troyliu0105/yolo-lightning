import cv2
import numpy as np
import torch
import torch.nn.functional as F
import torchvision.transforms.functional as trans_funcs
from PIL import Image, ImageOps
from numpy import random

__all__ = ['to_pic', 'to_tensor', 'horizontal_flip', 'resize_large_side', 'resize', 'random_resize',
           'pad_to_square']

mean = torch.as_tensor((0.485, 0.456, 0.406))[None, None].permute((2, 0, 1))
std = torch.as_tensor((0.229, 0.224, 0.225))[None, None].permute((2, 0, 1))


def to_tensor(pic):
    tensor = trans_funcs.to_tensor(pic)
    tensor = trans_funcs.normalize(tensor, mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), inplace=True)
    return tensor


def to_pic(tensor):
    tensor = tensor * std.to(dtype=tensor.dtype, device=tensor.device) + mean.to(dtype=tensor.dtype,
                                                                                 device=tensor.device)
    pic = trans_funcs.to_pil_image(tensor)
    return pic


def horizontal_flip(images, targets):
    images = torch.flip(images, [-1])
    targets[:, 2] = 1 - targets[:, 2]
    return images, targets


def resize_large_side(img, size):
    if isinstance(img, Image.Image):
        w, h = img.size
    else:
        h, w, _ = img.shape

    if h < w:
        h = int(np.round((size * h) / w))
        w = size
    else:
        w = int(np.round((size * w) / h))
        h = size
    return img.resize((w, h)) if isinstance(img, Image.Image) else cv2.resize(img, (w, h))


def resize(image, size):
    image = F.interpolate(image.unsqueeze(0), size=size, mode="nearest").squeeze(0)
    return image


def random_resize(images, min_size=288, max_size=448):
    new_size = random.sample(list(range(min_size, max_size + 1, 32)), 1)[0]
    images = F.interpolate(images, size=new_size, mode="nearest")
    return images


def pad_to_square(img, pad_value):
    if not isinstance(img, Image.Image) and isinstance(img, np.ndarray):
        img = Image.fromarray(img.astype(np.uint8))
        img, pad = _pad_to_square_pil(img, pad_value)
        img = np.array(img)
        return img, pad
    else:
        return _pad_to_square_pil(img, pad_value)


def _pad_to_square_torch(img, pad_value):
    c, h, w = img.shape
    dim_diff = np.abs(h - w)
    # (upper / left) padding and (lower / right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # Determine padding
    pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
    # Add padding
    img = F.pad(img, pad, "constant", value=pad_value)

    return img, pad


def _pad_to_square_pil(img: Image.Image, pad_value):
    w, h = img.size
    dim_diff = np.abs(h - w)
    # (upper / left) padding and (lower / right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # Determine padding
    pad = (0, pad1, 0, pad2) if h <= w else (pad1, 0, pad2, 0)
    # Add padding
    img = ImageOps.expand(img, border=pad, fill=pad_value)

    return img, pad
