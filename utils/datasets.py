import abc
import logging
import os
import random
from itertools import product
from multiprocessing import Value
from typing import List, Optional, Callable

import numpy as np
import torch
import torchvision.transforms as transforms
from PIL import Image
from imgaug.augmentables import BoundingBoxesOnImage
from torch.utils.data import Dataset, ConcatDataset
from torchvision.datasets import VOCDetection

from utils import xyxy2xywh, xywh2xyxy
from utils.augment import BBoxAugment
from utils.augment_snippets import pad_to_square, to_tensor, resize_large_side, to_pic
from utils.mixup import mix_them_up

__all__ = ['ImageFolder', 'ListDataset', 'PascalVOC', 'FaceFolder', 'get_datasets', 'Collater']


class Batch(object):
    def __init__(self, images, labels, sizes):
        self.images = images
        self.labels = labels
        self.sizes = sizes

    def pin_memory(self):
        self.images = self.images.pin_memory()
        self.labels = self.labels.pin_memory()
        return self

    def to(self, dtype):
        self.images = self.images.to(dtype=dtype)
        self.labels = self.labels.to(dtype=dtype)
        return self


class Collater(object):
    def __init__(self, multiscale=True, img_size=416):
        self.batch_count = Value('b', 0)
        self.multiscale = multiscale
        self.img_size = img_size
        self.min_size = self.img_size - 3 * 32
        self.max_size = self.img_size + 3 * 32

    def __call__(self, batch):
        imgs, targets, sizes = list(zip(*batch))
        # Remove empty placeholder targets
        imgs = [img for img in imgs if img is not None]
        targets = [boxes for boxes in targets if boxes is not None]
        sizes = [s for s in sizes if s is not None]
        # Add sample index to targets
        for i, boxes in enumerate(targets):
            boxes[:, 0] = i
        targets = torch.cat(targets, 0)
        # # Selects new image size every tenth batch
        # img_size = self.img_size
        # if self.multiscale:
        #     with self.batch_count.get_lock():
        #         self.batch_count.value += 1
        #         if self.batch_count.value % 10 == 0:
        #             img_size = random.choice(range(self.min_size, self.max_size + 1, 32))
        #             self.batch_count.value = 0
        #             self.img_size = img_size
        #             logging.debug(f"changing size to {img_size}")
        # # Resize images to input shape
        imgs = torch.stack(imgs)
        batch = Batch(images=imgs, labels=targets, sizes=sizes)
        return batch


class ImageFolder(Dataset):
    def __init__(self, folder_path, img_size=416):
        self.files = sorted([p.path for p in os.scandir(folder_path) if os.path.splitext(p.name)[1].lower()
                             in ('.jpg', '.jpeg', '.png')])
        logging.info(f'Validation folder has {len(self.files)} images')
        self.img_size = img_size
        self.to_tensor = transforms.ToTensor()

    def __getitem__(self, index):
        img_path = self.files[index % len(self.files)]
        # Extract image as PyTorch tensor
        img = Image.open(img_path)
        # Pad to square resolution
        img, _ = pad_to_square(img, 0)
        # Resize
        img = img.resize((self.img_size, self.img_size))
        img = self.to_tensor(img)
        return img_path, img

    def __len__(self):
        return len(self.files)


class BaseDataset(Dataset, metaclass=abc.ABCMeta):
    """
    Dataset base class
    """

    def __init__(self, img_size=416,
                 augmenters: Callable = None,
                 normalized_labels=True,
                 train=True,
                 mix_up=False,
                 label_smooth=False,
                 class_weights=None):
        """
        initialization dataset
        :param img_size:            size to rescale
        :param augments:            what kinds of augments to use
        :param normalized_labels:   whether bboxes are already normalized to (0, 1)
        :param train:               if not train, no augments will be used.
        """
        self.img_size = img_size
        self.normalized_labels = normalized_labels
        self.max_size = self.img_size + 3 * 32
        self.batch_count = 0
        self.augmenters = augmenters
        self.to_tensor = to_tensor
        self.mix_up = mix_up
        self.label_smooth = label_smooth

    # noinspection DuplicatedCode
    def post_precess(self, img: Image.Image, label: np.ndarray):
        img, label = self._denormalized_label(img, label)
        img, label = self.augmenters(img, label) if self.augmenters is not None else (img, label)
        img, label = self._pad_to_square(img, label) if img is not None else (None, None)
        img, label = self._normalized_label(img, label) if img is not None else (None, None)
        img = resize_large_side(img, self.max_size) if img is not None else None
        img, label = self._denormalized_label(img, label) if img is not None else (None, None)

        if img is None or label is None:
            return None, None
        else:
            # all strides should be positive
            img = np.ascontiguousarray(img, np.uint8)

        img = self.to_tensor(img)
        boxes = torch.from_numpy(label)
        targets = torch.zeros((boxes.size(0), 6))
        targets[:, 1:] = boxes
        return img, targets

    def __getitem__(self, idx):
        if self.mix_up:
            img, label = mix_them_up(self, idx, (0.45, 0.55))
        else:
            img, label = self.read_data_by_idx(idx)
        if img is None or label is None:
            return None, None
        if img.mode != 'RGB':
            img = img.convert('RGB')
        size = img.size
        img, label = self.post_precess(img, label)
        return img, label, size if img is not None else None

    def __len__(self):
        return self.dataset_len()

    def _pad_to_square(self, img, label):
        # =========================PADDING=====================================
        h, w, _ = img.shape
        # Pad to square resolution
        img, pad = pad_to_square(img, 0)
        padded_h, padded_w, _ = img.shape

        # Extract coordinates for unpadded + unscaled image
        x1, y1, x2, y2 = xywh2xyxy(label[..., 1:]).T
        # Adjust for added padding
        x1 += pad[0]
        y1 += pad[1]
        x2 += pad[0]
        y2 += pad[1]
        # Returns (x, y, w, h)
        label[..., 1:] = xyxy2xywh(np.vstack((x1, y1, x2, y2)).T)
        # =========================PADDING=====================================
        return img, label

    def _normalized_label(self, img, label):
        if label[..., 1:].max() < 1:
            return img, label
        if isinstance(img, Image.Image):
            w, h = img.size
        else:
            h, w, _ = img.shape
        label[..., [1, 3]] /= w
        label[..., [2, 4]] /= h
        return img, label

    def _denormalized_label(self, img, label):
        if label[..., 1:].max() > 1:
            return img, label
        if isinstance(img, Image.Image):
            w, h = img.size
        else:
            h, w, _ = img.shape
        label[..., [1, 3]] *= w
        label[..., [2, 4]] *= h
        return img, label

    @abc.abstractmethod
    def read_data_by_idx(self, idx) -> (Image.Image, np.ndarray):
        """
        read image and label,
        :param idx:
        :return:    (pil image, coordinates in xywh)
        """
        pass

    @abc.abstractmethod
    def dataset_len(self) -> int:
        pass


class ListDataset(BaseDataset):
    def __init__(self, list_path, **kwargs):
        super().__init__(**kwargs)
        with open(list_path, "r") as file:
            self.img_files = file.readlines()
        self.img_files = [os.path.expanduser(p) for p in self.img_files]
        self.label_files = []
        for path in self.img_files:
            path_seps = path.split('/')
            path_seps[-1] = os.path.splitext(path_seps[-1])[0] + '.txt'
            path_seps[-2] = 'labels'
            self.label_files.append('/'.join(path_seps))

    def read_data_by_idx(self, idx) -> (Image.Image, np.ndarray):
        img_path = self.img_files[idx % len(self.img_files)].rstrip()
        img = Image.open(img_path)

        label_path = self.label_files[idx % len(self.img_files)].rstrip()
        boxes = None
        if os.path.exists(label_path):
            boxes = np.loadtxt(label_path).reshape(-1, 5)

        return img, boxes

    def dataset_len(self):
        return len(self.img_files)


class PascalVOC(BaseDataset):
    def __init__(self, root, class_map, year='2012', image_set='train', keep_difficult=False, **kwargs):
        super(PascalVOC, self).__init__(**kwargs)
        self.voc = VOCDetection(root, year=year, image_set=image_set, download=False)
        self.keep_difficult = keep_difficult
        self.idx2clz = dict(enumerate(class_map))
        self.clz2idx = dict(zip(class_map, range(len(class_map))))

    def read_data_by_idx(self, idx) -> (Image.Image, np.ndarray):
        img, annotations = self.voc[idx]
        boxes = []
        objs = annotations['annotation']['object']
        if isinstance(objs, dict):
            b = objs['bndbox']
            if objs['difficult'] == '1' and not self.keep_difficult:
                return None, None
            boxes.append([self.clz2idx[objs['name']], b['xmin'], b['ymin'], b['xmax'], b['ymax']])
        else:
            for b in objs:
                if b['difficult'] == '1' and not self.keep_difficult:
                    continue
                name = b['name']
                b = b['bndbox']
                boxes.append([self.clz2idx[name], b['xmin'], b['ymin'], b['xmax'], b['ymax']])
        boxes = np.array(boxes).astype(np.float32)
        boxes[:, 1:] = xyxy2xywh(boxes[:, 1:])
        return img, boxes

    def dataset_len(self) -> int:
        return len(self.voc)


class FaceFolder(BaseDataset):
    def __init__(self, root, recursive=True, img_size=416, max_nrows=9):
        super().__init__(img_size, augments=None, normalized_labels=True, train=False)
        root = os.path.expanduser(root)
        combile_nums = list(range(1, max_nrows))
        if recursive:
            img_list = [os.path.join(root, f)
                        for root, _, files in os.walk(root)
                        for f in files if os.path.splitext(f)[1].lower() in ('.jpg', '.jpeg', '.png')]
        else:
            img_list = [f.path for f in os.scandir(root)
                        if os.path.splitext(f.name)[1].lower() in ('.jpg', '.jpeg', '.png')]
        random.shuffle(img_list)
        self.img_list = img_list
        self.img_chunks = []
        i = 0
        while i < len(img_list):
            nrows = np.random.choice(combile_nums)
            totals = nrows ** 2
            if i + totals > len(img_list):
                break
            self.img_chunks.append(img_list[i:i + totals])
            i += totals

    def read_data_by_idx(self, idx) -> (Image.Image, np.ndarray):
        imgs2read = self.img_chunks[idx]
        total = len(imgs2read)
        nrows = int(np.sqrt(total))
        chunk_size = int(np.ceil(self.img_size / nrows))
        imgs = [Image.open(f).resize((chunk_size, chunk_size)) for f in imgs2read]
        boxs = [np.array([0.5, 0.5, np.random.uniform(0.9, 1.0), np.random.uniform(0.9, 1.0)])
                for _ in range(total)]
        boxs = [xywh2xyxy(b) for b in boxs]
        size = self.img_size
        img = Image.new('RGB', size=(size, size))
        for (i, j), im, b in zip(list(product(range(0, size, chunk_size), range(0, size, chunk_size))),
                                 imgs,
                                 boxs):
            img.paste(im, (i, j))
            b /= nrows
            b[[0, 2]] += (i / size)
            b[[1, 3]] += (j / size)
        boxs = np.array(boxs)
        boxs = xyxy2xywh(boxs)
        label = np.zeros((boxs.shape[0], 1))
        boxs = np.hstack((label, boxs))
        return img, boxs

    def dataset_len(self) -> int:
        return len(self.img_chunks)


def get_datasets(config):
    train_dataset = None
    val_dataset = None
    dataset_cfg = config['dataset']
    augments = dataset_cfg['augments']
    if augments is None or not augments:
        augments = []
    t_aug = BBoxAugment(augments, config['yolo']['im_size'] + 32 * 3)
    v_aug = BBoxAugment([], config['yolo']['im_size'] + 32 * 3)
    if dataset_cfg['type'] == 'list':
        train_dataset = ListDataset(os.path.expanduser(dataset_cfg['train']),
                                    img_size=config['yolo']['im_size'],
                                    augments=dataset_cfg['augments'],
                                    train=True,
                                    mix_up=dataset_cfg['mix_up'])
        val_dataset = ListDataset(os.path.expanduser(dataset_cfg['valid']),
                                  img_size=config['yolo']['im_size'],
                                  train=False)
    elif dataset_cfg['type'] == 'pascal':
        from utils.snippets import load_classes
        class_map, _ = load_classes(dataset_cfg['class_path'])
        train_dataset = ConcatDataset([
            PascalVOC(os.path.expanduser(dataset_cfg['root']), class_map, year='2012', image_set='trainval',
                      # augments=dataset_cfg['augments'],
                      augmenters=t_aug,
                      normalized_labels=False,
                      train=True,
                      mix_up=dataset_cfg['mix_up']),
            PascalVOC(os.path.expanduser(dataset_cfg['root']), class_map, year='2007', image_set='trainval',
                      # augments=dataset_cfg['augments'],
                      augmenters=v_aug,
                      normalized_labels=False,
                      train=True,
                      mix_up=dataset_cfg['mix_up'])
        ])
        val_dataset = PascalVOC(os.path.expanduser(dataset_cfg['root']), class_map, year='2007', image_set='test',
                                augmenters=None, normalized_labels=False, train=False)
    elif dataset_cfg['type'] == 'coco':
        def _convert_coco_box(segments):
            pass

        from torchvision.datasets import CocoDetection
        train_root = dataset_cfg['train_root']
        train_anno = dataset_cfg['train_anno']
        valid_root = dataset_cfg['valid_root']
        valid_anno = dataset_cfg['train_anno']
        train_dataset = CocoDetection(train_root, train_anno, target_transform=_convert_coco_box)
        val_dataset = CocoDetection(valid_root, valid_anno, target_transform=_convert_coco_box)
    return train_dataset, val_dataset
