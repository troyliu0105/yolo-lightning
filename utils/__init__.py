from itertools import product
from typing import Union, List, Optional, Tuple

import numpy as np
import torch
from PIL import ImageDraw, Image
from torch import Tensor

__all__ = ['xywh2xyxy', 'xyxy2xywh', 'bbox_wh_iou', 'bbox_iou',
           'non_max_suppression', 'rescale_boxes']


def draw_boxes_on_images(images: List[Image.Image], detections: List[Optional[np.ndarray]],
                         class_map=None, color_map=None):
    for i, dect in enumerate(detections):
        if dect is not None:
            unique_labels = np.unique(dect[:, -1])
            n_cls_preds = len(unique_labels)
            img = images[i]
            drawer = ImageDraw.Draw(img, mode='RGB')
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in dect:
                conf = conf * cls_conf
                x1, y1, x2, y2 = [int(x) for x in (x1, y1, x2, y2)]
                drawer.rectangle((x1, y1, x2, y2),
                                 outline='red' if color_map is None else color_map[int(cls_pred)],
                                 width=int(conf * 4))
                if class_map:
                    msg = f'{class_map[int(cls_pred)]}:{conf:.2f}'
                    w, h = drawer.textsize(msg)
                    drawer.text((x1, y1 - h), msg, fill='red' if color_map is None else color_map[int(cls_pred)])
    return images


def make_grid(images: List[Union[Image.Image, np.ndarray]]) -> np.ndarray:
    images = [np.array(x) for x in images if isinstance(x, Image.Image)]
    h, w = images[0].shape[:2]
    side = int(np.floor(np.sqrt(len(images))))
    out = np.empty(shape=(int(side * h), int(side * w), 3), dtype=np.uint8)
    for (i, j), im in zip(list(product(range(0, out.shape[0], h), range(0, out.shape[1], w))), images):
        out[i:i + h, j: j + h] = im
    return out


def xywh2xyxy(x: Union[Tensor, np.ndarray]) -> Union[Tensor, np.ndarray]:
    if isinstance(x, torch.Tensor):
        y = torch.empty_like(x)
    else:
        y = np.zeros_like(x)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    y[..., 1] = x[..., 1] - x[..., 3] / 2
    y[..., 2] = x[..., 0] + x[..., 2] / 2
    y[..., 3] = x[..., 1] + x[..., 3] / 2
    return y


def xyxy2xywh(x: Union[Tensor, np.ndarray]) -> Union[Tensor, np.ndarray]:
    if isinstance(x, torch.Tensor):
        y = torch.empty_like(x)
    else:
        y = np.zeros_like(x)
    y[..., 0] = (x[..., 2] + x[..., 0]) / 2
    y[..., 1] = (x[..., 3] + x[..., 1]) / 2
    y[..., 2] = x[..., 2] - x[..., 0]
    y[..., 3] = x[..., 3] - x[..., 1]
    return y


# noinspection DuplicatedCode
def bbox_wh_iou(wh1, wh2):
    """
    compute ious for anchor and gts
    :param wh1: anchor
    :param wh2: ground truth boxes
    :return: (N_gt))
    """
    wh2 = wh2.t()
    w1, h1 = wh1[0], wh1[1]
    w2, h2 = wh2[0], wh2[1]
    inter_area = torch.min(w1, w2) * torch.min(h1, h2)
    union_area = w1 * h1 + w2 * h2 - inter_area
    return inter_area / union_area


# noinspection DuplicatedCode
def bbox_iou(box1: Union[np.ndarray, Tensor], box2: Union[np.ndarray, Tensor], x1y1x2y2=True):
    """
    Returns the IoU of two bounding boxes
    """
    assert box1.dtype == box2.dtype
    if isinstance(box1, np.ndarray):
        _min = np.minimum
        _max = np.maximum
        _clip = lambda x, min: np.clip(x, a_min=min, a_max=x.max())
    else:
        _min = torch.min
        _max = torch.max
        _clip = torch.clamp
    if not x1y1x2y2:
        # Transform from center and width to exact coordinates
        box1 = xywh2xyxy(box1)
        box2 = xywh2xyxy(box2)

    # Get the coordinates of bounding boxes
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    # get the corrdinates of the intersection rectangle
    inter_rect_x1 = _max(b1_x1, b2_x1)
    inter_rect_y1 = _max(b1_y1, b2_y1)
    inter_rect_x2 = _min(b1_x2, b2_x2)
    inter_rect_y2 = _min(b1_y2, b2_y2)
    # Intersection area
    inter_area = _clip(inter_rect_x2 - inter_rect_x1, min=0) * _clip(inter_rect_y2 - inter_rect_y1, min=0)
    # Union Area
    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou


def bbox_iou_recurse(box1: Tensor, box2: Tensor, x1y1x2y2=True):
    assert box1.dtype == box2.dtype
    if not x1y1x2y2:
        box1 = xywh2xyxy(box1)
        box2 = xywh2xyxy(box2)
    s1 = box1.size(0)
    s2 = box2.size(0)
    max_xy = torch.min(box1[:, 2:].unsqueeze(1).expand(s1, s2, 2),
                       box2[:, 2:].unsqueeze(0).expand(s1, s2, 2))
    min_xy = torch.max(box1[:, :2].unsqueeze(1).expand(s1, s2, 2),
                       box2[:, :2].unsqueeze(0).expand(s1, s2, 2))
    inter = torch.clamp(max_xy - min_xy, min=0)
    inter = inter[..., 0] * inter[..., 1]
    area_1 = ((box1[..., 2] - box1[..., 0]) * (box1[..., 3] - box1[..., 1])).unsqueeze(1).expand_as(inter)
    area_2 = ((box2[..., 2] - box2[..., 0]) * (box2[..., 3] - box2[..., 1])).unsqueeze(0).expand_as(inter)
    union = area_1 + area_2 - inter
    iou = inter / (union + 1e-16)
    return iou


def top_k_from_detections(detections: List[Tensor], top_k: int):
    """
    take top-k of each image
    :param detections: processed from nms. already sorted..
    :param top_k:
    :return:
    """
    top_ks = []
    for detection in detections:
        if detection is None or detection.shape[0] <= top_k:
            top_ks.append(detection)
            continue
        # _, sorted_idx = torch.sort(detection[..., 4], descending=True)
        # sorted_detects = detection[sorted_idx]
        top_ks.append(detection[:top_k])
    return top_ks


def non_max_suppression(prediction: Tensor, conf_thresh=0.5, nms_threshold=0.4, soft=False) \
        -> List[Optional[Tensor]]:
    """
    Removes detections with lower object confidence score than 'conf_thresh' and performs
    Non-Maximum Suppression to further filter detections.
    :param prediction:  (center x, center y, width, height, object_conf, class_score, class_pred)
    :param conf_thresh: ignore conf under conf_thresh
    :param nms_threshold:   ignore iou larger than `nms_threshold`
    :param soft: whether use soft nms
    :return: (x1, y1, x2, y2, object_conf, class_score, class_pred)
    """

    # From (center x, center y, width, height) to (x1, y1, x2, y2)
    prediction[..., :4] = xywh2xyxy(prediction[..., :4])
    output = [None for _ in range(prediction.size(0))]
    for image_i, image_pred in enumerate(prediction):
        # Filter out confidence scores below threshold
        image_pred = image_pred[image_pred[:, 4] >= conf_thresh]
        # If none are remaining => process next image
        if not image_pred.shape[0]:
            continue
        # Object confidence times class confidence
        score = image_pred[:, 4] * image_pred[:, 5:].max(1)[0]
        # Sort by it
        image_pred = image_pred[(-score).argsort()]
        class_confs, class_preds = image_pred[:, 5:].max(1, keepdim=True)
        # (nBox, (x1, y1, x2, y2, obj_conf, pred_conf, pred))
        detections = torch.cat((image_pred[:, :5], class_confs.float(), class_preds.float()), 1)
        # Perform non-maximum suppression
        keep_boxes = []
        while detections.shape[0]:
            ious = bbox_iou(detections[0, :4].unsqueeze(0), detections[:, :4])
            large_overlap = ious > nms_threshold
            label_match = detections[0, -1] == detections[:, -1]
            # Indices of boxes with lower confidence scores, large IOUs and matching labels
            invalid = large_overlap & label_match
            if soft:
                invalid_ious = ious[invalid]
                detections[invalid] *= (1 - invalid_ious)
            else:
                weights = detections[invalid, 4:5]
                # Merge overlapping bboxes by order of confidence
                detections[0, :4] = (weights * detections[invalid, :4]).sum(0) / weights.sum()
            keep_boxes += [detections[0]]
            if not soft:
                detections = detections[~invalid]
        if keep_boxes:
            output[image_i] = torch.stack(keep_boxes)

    return output


def rescale_boxes(boxes: Union[Tensor, np.ndarray], current_dim: int, original_shape: Tuple[int, int]) \
        -> Union[Tensor, np.ndarray]:
    """
    Rescales bounding boxes to the original shape
    :param boxes:
    :param current_dim:
    :param original_shape:
    :return:
    """
    orig_w, orig_h = original_shape
    # The amount of padding that was added
    pad_x = max(orig_h - orig_w, 0) * (current_dim / max(original_shape))
    pad_y = max(orig_w - orig_h, 0) * (current_dim / max(original_shape))
    # Image height and width after padding is removed
    unpad_h = current_dim - pad_y
    unpad_w = current_dim - pad_x
    # Rescale bounding boxes to dimension of original image
    boxes[:, 0] = ((boxes[:, 0] - pad_x // 2) / unpad_w) * orig_w
    boxes[:, 1] = ((boxes[:, 1] - pad_y // 2) / unpad_h) * orig_h
    boxes[:, 2] = ((boxes[:, 2] - pad_x // 2) / unpad_w) * orig_w
    boxes[:, 3] = ((boxes[:, 3] - pad_y // 2) / unpad_h) * orig_h
    return boxes
