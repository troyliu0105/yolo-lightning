import imgaug
import numpy as np
import random
# noinspection PyUnresolvedReferences
from PIL import Image
from imgaug import augmenters as iaa
from imgaug.augmentables import BoundingBox, BoundingBoxesOnImage
from numpy.random import RandomState

from utils import xywh2xyxy

__all__ = ['BBoxAugment']


class BBoxAugment(object):
    def __init__(self, augments, out_size):
        self.out_size = out_size
        sometimes = lambda aug: iaa.Sometimes(0.5, aug)
        all_augments = {
            'hue_and_saturation': sometimes(iaa.AddToHueAndSaturation(value=(-50, 50), per_channel=True)),
            'affine': sometimes(iaa.AffineCv2(translate_percent={'x': 0.1, 'y': 0.1}, rotate=(-10, 10))),
            'flip': iaa.Fliplr(p=0.5),
            'multiply': sometimes(iaa.Multiply((0.5, 1.5), per_channel=True)),
            'coarse_salt_pepper': iaa.Sometimes(0.15, iaa.CoarseSaltAndPepper(p=0.15, size_percent=(0.4, 0.7))),
            'salt_and_pepper': iaa.Sometimes(0.15, iaa.SaltAndPepper(p=0.15)),
            'blur': sometimes(iaa.OneOf([
                iaa.GaussianBlur((0, 1.0)),
                iaa.AverageBlur(k=(1, 3)),
                iaa.MotionBlur()])),
            'sharp': sometimes(iaa.Sharpen(alpha=(0, 0.5), lightness=(0.75, 1.5))),
            'noise': sometimes(iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.07 * 255), per_channel=0.5)),
            'expand': sometimes(Expand())
        }
        aug_list = [all_augments[name] for name in augments]
        self.seq = iaa.Sequential(aug_list, random_order=True, name='BBox Augments',
                                  random_state=imgaug.iarandom.convert_seed_to_generator(random.randint(1, 1e+10)))

    def __call__(self, image, label):
        label[:, 1:] = xywh2xyxy(label[:, 1:])
        if not isinstance(image, np.ndarray):
            image = np.array(image)
        h, w, _ = image.shape
        bbs = BoundingBoxesOnImage(
            [BoundingBox(b[1], b[2], b[3], b[4], label=b[0])
             for b in label], shape=(h, w))
        image_aug, label_aug = self.seq(image=image, bounding_boxes=bbs)
        max_side = max(image_aug.shape[:2])
        # image_aug, label_aug = iaa.PadToFixedSize(width=max_side, height=max_side,
        #                                           position="center")(image=image_aug, bounding_boxes=label_aug)
        # image_aug, label_aug = iaa.Resize(self.out_size, imgaug.ALL)(image=image_aug, bounding_boxes=label_aug)
        label_aug = label_aug.clip_out_of_image()
        h, w, _ = image_aug.shape
        label_aug = [[b.label, b.center_x, b.center_y, b.width, b.height] for b in label_aug.bounding_boxes]
        label_aug = np.array(label_aug).astype(np.float)
        # label is totally out of img
        if label_aug.size == 0:
            return None, None
        return image_aug, label_aug


class Expand(iaa.Augmenter):
    def _augment_images(self, images, random_state: RandomState, parents, hooks):
        for i, img in enumerate(images):
            height, width, depth = img.shape
            ratio = random_state.uniform(1, 4)
            left = random_state.uniform(0, width * ratio - width)
            top = random_state.uniform(0, height * ratio - height)

            expand_image = np.zeros(
                (int(height * ratio), int(width * ratio), depth),
                dtype=img.dtype)
            expand_image[:, :, :] = random_state.randint(0, 255, depth)
            expand_image[int(top):int(top + height), int(left):int(left + width)] = img
            images[i] = expand_image
        return images

    def _augment_heatmaps(self, heatmaps, random_state, parents, hooks):
        return heatmaps

    def _augment_keypoints(self, keypoints_on_images, random_state, parents, hooks):
        for i, kp in enumerate(keypoints_on_images):
            height, width = kp.height, kp.width
            ratio = random_state.uniform(1, 4)
            left = random_state.uniform(0, width * ratio - width)
            top = random_state.uniform(0, height * ratio - height)
            kp = kp.shift(left, top)
            keypoints_on_images[i] = kp
        return keypoints_on_images

    def get_parameters(self):
        pass
