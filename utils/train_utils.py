import logging
import os
import random
import shutil
import sys

import coloredlogs
import torch
import torch.optim as optim
from ignite.engine import Engine, convert_tensor
from prefetch_generator import BackgroundGenerator
from torch import distributed
from torch.nn.functional import interpolate
from torch.nn.utils import clip_grad_value_
from torch.utils.data import DataLoader

from models.loss import YOLOLoss
from models.net import YOLONet
from utils.datasets import get_datasets, Collater
from utils.snippets import is_distributed, defined, load_cfg, dump_cfg


class DataLoaderPrefetch(DataLoader):
    def __iter__(self):
        return BackgroundGenerator(super(DataLoaderPrefetch, self).__iter__(), max_prefetch=3)


def _prepare_batch(engine: Engine, batch, base_size, multiscale=False, device=None, non_blocking=False):
    """Prepare batch for training: pass to a device with options.

    """
    x, y = batch.images, batch.labels
    min_size = base_size - 32 * 3
    max_size = base_size + 32 * 3
    if multiscale:
        if engine.state.iteration % 10 == 0:
            current_size = random.choice(range(min_size, max_size, 32))
            engine.last_size = current_size
            logging.debug(f'Resizing to {current_size}')
        else:
            current_size = engine.last_size if hasattr(engine, 'last_size') else base_size
    else:
        current_size = base_size
    y[..., -4:] *= (current_size / max_size)
    x = interpolate(x, current_size)
    return (convert_tensor(x, device=device, non_blocking=non_blocking),
            convert_tensor(y, device=device, non_blocking=non_blocking))


def get_train_batch_fn(cfg, model, optimizer, loss_fn, device, non_blocking=True):
    accumulate = 1 if not defined(cfg['optimizer'], 'accumulate') else cfg['optimizer']['accumulate']
    clip = None if not defined(cfg['optimizer'], 'clip') else cfg['optimizer']['clip']
    # cpu = cfg['cpu']
    is_amp_enabled = cfg['train']['amp']['enable']
    if is_amp_enabled:
        try:
            from apex import amp
        except ImportError:
            logging.error('APEX is not installed, fp16 mixing training disabled')
            is_amp_enabled = False

    def _train(engine: Engine, batch):
        model.train()
        x, y = _prepare_batch(engine, batch, cfg['yolo']['im_size'], multiscale=True,
                              device=device, non_blocking=non_blocking)
        sizes = batch.sizes
        y_pred = model(x)
        outputs, losses, metrics = loss_fn(feats=y_pred, target=y)
        losses = [l / accumulate for l in losses]
        if not is_amp_enabled:
            losses = sum(losses)
            losses.backward()
        else:
            last_idx = len(losses) - 1
            for idx, l in enumerate(losses):
                with amp.scale_loss(l, optimizer, loss_id=idx) as scaled_loss:
                    scaled_loss.backward(retain_graph=idx != last_idx)
        if engine.state.iteration % accumulate == 0:
            if clip is not None:
                if not is_amp_enabled:
                    for param_group in optimizer.param_groups:
                        clip_grad_value_(param_group['params'], clip)
                else:
                    clip_grad_value_(amp.master_params(optimizer), clip)
            optimizer.step()
            optimizer.zero_grad()
        return {
            **metrics,
            'x': x.detach().cpu(),
            'y_hat': outputs.detach().cpu(),
            'y': y.detach().cpu(),
            'sizes': sizes
        }

    return _train


def get_evaluator_fn(cfg, model, loss_fn, device, non_blocking=True):
    # cpu = cfg['cpu']

    def _evaluate(engine: Engine, batch):
        model.eval()
        with torch.no_grad():
            x, y = _prepare_batch(engine, batch, cfg['yolo']['im_size'], multiscale=False,
                                  device=device, non_blocking=non_blocking)
            sizes = batch.sizes
            y_pred = model(x)
            outputs, _, metrics = loss_fn(feats=y_pred, target=y)
        return {
            **metrics,
            'x': x.detach().cpu(),
            'y_hat': outputs.detach().cpu(),
            'y': y.detach().cpu(),
            'sizes': sizes
        }

    return _evaluate


def prepare_cfg(cfg, name, level, train=True):
    cfg = load_cfg(cfg)
    coloredlogs.install(level=getattr(logging, level.upper()),
                        fmt='%(levelname)s\t[%(asctime)s %(filename)s] %(message)s')
    if not train:
        return cfg
    train_cfg = cfg['train']
    detect_cfg = cfg['yolo']
    model_name = detect_cfg['model_params']['backbone']
    epochs = f"-epochs{train_cfg['epochs']}"
    batch = f"-batch{train_cfg['batch_size']}{'x' + str(distributed.get_world_size()) if is_distributed() else ''}"
    accumulate = cfg['optimizer']['accumulate']
    accumulate = f"{'-accumulate' + str(accumulate) if accumulate > 1 else ''}"
    sub_working_dir = f'{os.path.curdir}' \
                      f'/experiments' \
                      f"/{'yolo' if defined(cfg, 'yolo') else 'ssd'}-{name}/{model_name}" \
                      f"-size{detect_cfg['im_size']}x{detect_cfg['im_size']}" \
                      f"{epochs}" \
                      f"{batch}" \
                      f"{accumulate}" \
                      f"{'-fp32' if not train_cfg['amp']['enable'] else '-fp16'}" \
                      f"-{train_cfg['try']}" \
                      f"{'-rank' + str(distributed.get_rank()) if is_distributed() else ''}"
    if os.path.exists(sub_working_dir):
        if level.upper() == 'DEBUG':
            shutil.rmtree(sub_working_dir)
            os.makedirs(sub_working_dir)
        elif input(f'experiments exists, delete?\n\t{sub_working_dir}: y/N').lower() == 'y':
            shutil.rmtree(sub_working_dir)
            os.makedirs(sub_working_dir)
    else:
        os.makedirs(sub_working_dir, exist_ok=True)
    cfg['sub_working_dir'] = sub_working_dir
    logging.info('sub working dir: %s' % sub_working_dir)
    logging.info(f'Use tensorboard as \"tensorboard --logdir={sub_working_dir}/log\"')
    cfg_bak = dump_cfg(sub_working_dir, cfg, ext='toml')
    logging.info(f'Backup config to {cfg_bak}')

    cpu = torch.device('cpu')
    cfg['cpu'] = cpu
    return cfg


def get_dataloader(cfg):
    detect_cfg = cfg['yolo'] if defined(cfg, 'yolo') else cfg['ssd']
    dataset, val_dataset = get_datasets(cfg)
    if sys.platform == 'darwin':
        # use tiny datasets on my macbook
        from torch.utils.data import Subset
        dataset = Subset(dataset, list(range(32)))
        val_dataset = Subset(dataset, list(range(32)))
    # train_sampler = torch.utils.data.DistributedSampler(dataset, distributed.get_world_size(),
    #                                                     distributed.get_rank()) if distributed_is_availabel() else None
    # val_sampler = torch.utils.data.DistributedSampler(val_dataset, distributed.get_world_size(),
    #                                                   distributed.get_rank()) if distributed_is_availabel() else None
    train_sampler = None
    val_sampler = None
    dataloader = DataLoaderPrefetch(dataset,
                                    batch_size=cfg['train']['batch_size'],
                                    shuffle=(train_sampler is None),
                                    num_workers=cfg['train']['num_workers'],
                                    pin_memory=cfg['train']['pin_memory'],
                                    collate_fn=Collater(multiscale=cfg['dataset']['multiscale'],
                                                        img_size=detect_cfg['im_size']),
                                    sampler=train_sampler,
                                    drop_last=True)
    val_dataloader = DataLoaderPrefetch(val_dataset,
                                        batch_size=cfg['train']['batch_size'],
                                        shuffle=True,
                                        num_workers=cfg['train']['num_workers'],
                                        pin_memory=cfg['train']['pin_memory'],
                                        collate_fn=Collater(multiscale=False,
                                                            img_size=detect_cfg['im_size']),
                                        sampler=val_sampler,
                                        drop_last=False)
    return dataloader, val_dataloader


def get_optimizer(config, net: torch.nn.Module):
    # Assign different lr for each layer
    base_params = list([p[1] for p in net.named_parameters() if p[0].startswith('backbone')])
    logits_params = list([p[1] for p in net.named_parameters() if not p[0].startswith('backbone')])
    optimizer_cfg = config['optimizer']
    if not optimizer_cfg['freeze_backbone']:
        params = [*base_params, *logits_params]
    else:
        logging.info("freeze backbone's parameters.")
        backbone = dict(net.named_modules())['backbone']
        bn_parameters = []
        for name, p in backbone.named_parameters():
            if 'bn' in name:
                bn_parameters.append(p)
            else:
                p.requires_grad = False

        # for p in backbone.named_parameters():
        #     p.requires_grad = False
        params = [*bn_parameters, *logits_params]

    # Initialize optimizer class
    optimizer_type = optimizer_cfg['type']
    lr = optimizer_cfg['lr']
    if optimizer_type == 'adamw':
        optimizer = optim.AdamW(params, lr=lr, weight_decay=config['optimizer']['weight_decay'])
    elif optimizer_type == 'amsgrad':
        optimizer = optim.AdamW(params, lr=lr, weight_decay=config['optimizer']['weight_decay'],
                                amsgrad=True)
    elif optimizer_type == 'rmsprop':
        optimizer = optim.RMSprop(params, lr=lr, weight_decay=config['optimizer']['weight_decay'])
    elif optimizer_type == 'swats':
        import swats
        optimizer = swats.SWATS(params, lr=lr, weight_decay=config['optimizer']['weight_decay'], verbose=True,
                                amsgrad=True)
    else:
        # Default to sgd
        logging.info('Using SGD optimizer.')
        optimizer = optim.SGD(params,
                              lr=lr,
                              momentum=0.9,
                              weight_decay=optimizer_cfg['weight_decay'],
                              nesterov=(optimizer_cfg['type'] == 'nesterov'))

    return optimizer


def make_model_optimizer_loss_fn(cfg):
    if defined(cfg, 'yolo'):
        model = YOLONet(cfg['yolo'])
        optimizer = get_optimizer(cfg, model)
        loss_fn = YOLOLoss(cfg['yolo'])
    else:
        raise ValueError('cfg should have [ssd] or [yolo]')
    return model, optimizer, loss_fn


def wrap_model_optimizer(model, optimizer, yolo_loss_fn, cfg, device):
    model.to(device)
    if defined(cfg['train'], 'amp') and cfg['train']['amp']['enable']:
        try:
            from apex import amp
            opt_level = cfg['train']['amp']['level']
            [model, yolo_loss_fn], optimizer = amp.initialize([model, yolo_loss_fn], optimizer, opt_level=opt_level,
                                                              loss_scale='dynamic',
                                                              num_losses=3, verbosity=1,
                                                              cast_model_outputs=torch.float32)
            if is_distributed():
                from apex import parallel
                model = parallel.convert_syncbn_model(model)
                model = parallel.DistributedDataParallel(model)
        except ImportError:
            logging.error(f'apex enabled, but no apex lib installed')
    else:
        if is_distributed():
            from torch import nn
            # model = nn.SyncBatchNorm.convert_sync_batchnorm(model)
            # model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[distributed.get_rank()],
            #                                                   output_device=distributed.get_rank())
            model = nn.parallel.DistributedDataParallel(model)

    return model, optimizer, yolo_loss_fn
