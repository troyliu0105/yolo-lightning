import argparse
import logging

import torch
from ignite.contrib.handlers import ProgressBar, TensorboardLogger, CustomPeriodicEvent, LRScheduler, \
    create_lr_scheduler_with_warmup
from ignite.contrib.handlers.tensorboard_logger import WeightsHistHandler
from ignite.engine import Engine, Events
from ignite.handlers import ModelCheckpoint
from ignite.metrics import RunningAverage
from torch import distributed
from torch.optim.lr_scheduler import MultiStepLR
from torchsummary import summary

from utils.ignite_extends import mAPMetric, ImageOutputHandler
from utils.snippets import load_classes, defined, is_distributed, select_device
from utils.train_utils import prepare_cfg, get_dataloader, get_train_batch_fn, get_evaluator_fn, \
    wrap_model_optimizer, make_model_optimizer_loss_fn


# noinspection PyUnboundLocalVariable
def create_trainer_evaluator(cfg, model, optimizer, yolo_loss_fn, train_dl, val_dl, device):
    class_map, color_map = load_classes(cfg['dataset']['class_path'])
    trainer = Engine(get_train_batch_fn(cfg, model, optimizer, yolo_loss_fn, device, non_blocking=True))
    evaluator = Engine(get_evaluator_fn(cfg, model, yolo_loss_fn, device, non_blocking=True))
    metrics = {
        'ap': mAPMetric(iou_ignore=cfg['eval']['iou_ignore'],
                        conf_threshold=cfg['eval']['nms_conf'],
                        nms_threshold=cfg['eval']['nms_threshold'],
                        top_k=cfg['eval']['top_k'],
                        soft=cfg['eval']['nms_soft'])
    }
    for name, metric in metrics.items():
        metric.attach(evaluator, name)

    train_avg_loss = RunningAverage(output_transform=lambda output: output['loss'])
    eval_avg_loss = RunningAverage(output_transform=lambda output: output['loss'])
    train_avg_loc = RunningAverage(output_transform=lambda output: output['loc'])
    eval_avg_loc = RunningAverage(output_transform=lambda output: output['loc'])
    train_avg_cls = RunningAverage(output_transform=lambda output: output['cls'])
    eval_avg_cls = RunningAverage(output_transform=lambda output: output['cls'])

    log_interval = cfg['train']['log_step_interval']
    model_checkpoint = ModelCheckpoint(f'{cfg["sub_working_dir"]}/checkpoints',
                                       filename_prefix='ckpt',
                                       save_interval=cfg['train']['checkpoints_epoch_interval'],
                                       n_saved=cfg['train']['save_num'],
                                       require_empty=True)
    pbar = ProgressBar(persist=True)
    pbar_eval = ProgressBar(persist=True, desc='Validating', smoothing=0.5, )
    writer = TensorboardLogger(f'{cfg["sub_working_dir"]}/log')
    log_period = CustomPeriodicEvent(n_epochs=log_interval)
    scheduler_params = cfg['train']['lr_scheduler'] if defined(cfg['train'], 'lr_scheduler') else None
    if scheduler_params is not None:
        milestones = scheduler_params['decay_steps']
        batchs_per_epoch = len(train_dl.dataset) // cfg['train']['batch_size']
        milestones = [m * batchs_per_epoch for m in milestones]
        lr_s = MultiStepLR(optimizer, milestones=milestones,
                           gamma=scheduler_params['decay_gamma'])

        if defined(scheduler_params, 'warmup'):
            warmup = scheduler_params['warmup']
            lr_s = create_lr_scheduler_with_warmup(lr_s,
                                                   warmup_start_value=warmup['from'],
                                                   warmup_end_value=cfg['optimizer']['lr'],
                                                   warmup_duration=warmup['steps'] * batchs_per_epoch)
        else:
            lr_s = LRScheduler(lr_s)
        trainer.add_event_handler(Events.ITERATION_STARTED, lr_s)
    train_image_output = ImageOutputHandler('train/predict', class_map, color_map,
                                            cfg['eval']['nms_conf'],
                                            cfg['eval']['nms_threshold'],
                                            cfg['eval']['nms_soft'],
                                            top_k=cfg['eval']['top_k'])
    eval_image_output = ImageOutputHandler('eval/predict', class_map, color_map,
                                           cfg['eval']['nms_conf'],
                                           cfg['eval']['nms_threshold'],
                                           cfg['eval']['nms_soft'],
                                           top_k=cfg['eval']['top_k'])

    # apply handlers
    pbar_metrics = ['loss', 'loc', 'cls']
    train_avg_conf = RunningAverage(output_transform=lambda output: output['conf'])
    train_avg_obj = RunningAverage(output_transform=lambda output: output['metrics/conf_obj'])
    train_avg_noobj = RunningAverage(output_transform=lambda output: output['metrics/conf_noobj'])
    train_avg_cls_acc = RunningAverage(output_transform=lambda output: output['metrics/cls_acc'])
    eval_avg_conf = RunningAverage(output_transform=lambda output: output['conf'])
    eval_avg_obj = RunningAverage(output_transform=lambda output: output['metrics/conf_obj'])
    eval_avg_noobj = RunningAverage(output_transform=lambda output: output['metrics/conf_noobj'])
    eval_avg_cls_acc = RunningAverage(output_transform=lambda output: output['metrics/cls_acc'])
    train_avg_conf.attach(trainer, 'conf')
    train_avg_obj.attach(trainer, 'obj')
    train_avg_noobj.attach(trainer, 'noobj')
    train_avg_cls_acc.attach(trainer, 'cls_acc')
    eval_avg_conf.attach(evaluator, 'conf')
    eval_avg_obj.attach(evaluator, 'obj')
    eval_avg_noobj.attach(evaluator, 'noobj')
    eval_avg_cls_acc.attach(evaluator, 'cls_acc')
    pbar_metrics.append('conf')
    log_period.attach(trainer)
    train_avg_loss.attach(trainer, 'loss')
    train_avg_loc.attach(trainer, 'loc')
    train_avg_cls.attach(trainer, 'cls')
    pbar.attach(trainer, metric_names=pbar_metrics)
    writer.attach(trainer, WeightsHistHandler(model),
                  getattr(log_period.Events, f'EPOCHS_{log_interval}_COMPLETED'))

    eval_avg_loss.attach(evaluator, 'loss')
    eval_avg_loc.attach(evaluator, 'loc')
    eval_avg_cls.attach(evaluator, 'cls')
    pbar_eval.attach(evaluator, metric_names=pbar_metrics)
    trainer.add_event_handler(Events.EPOCH_COMPLETED, model_checkpoint, {
        'model': model if not is_distributed() else model.module,
        'optimizer': optimizer
    })

    @trainer.on(Events.EPOCH_COMPLETED)
    def plot_avgs_on_epoch_end(engine: Engine):
        train_m = engine.state.metrics
        epoch = engine.state.epoch
        if epoch % log_interval == 0:
            eval_state = evaluator.run(val_dl)
            eval_m = eval_state.metrics
            train_image_output(trainer, writer, None)
            eval_image_output(evaluator, writer, None)
            writer.writer.add_scalars('loss/all', {'eval': eval_m['loss']},
                                      global_step=epoch)
            writer.writer.add_scalars('loss/loc', {'eval': eval_m['loc']},
                                      global_step=epoch)
            writer.writer.add_scalars('loss/conf', {'eval': eval_m['conf']},
                                      global_step=epoch)
            writer.writer.add_scalars('loss/cls', {'eval': eval_m['cls']},
                                      global_step=epoch)
            writer.writer.add_scalars('metrics/obj', {'eval': eval_m['obj']},
                                      global_step=epoch)
            writer.writer.add_scalars('metrics/noobj', {'eval': eval_m['noobj']},
                                      global_step=epoch)
            writer.writer.add_scalars('metrics/acc', {'eval': eval_m['cls_acc']},
                                      global_step=epoch)
            cls_metrics = {int(x[0]): x[1:] for x in eval_m['ap'][0]}
            mAP = eval_m['ap'][1]
            aps = {}
            ps = {}
            rs = {}
            f1s = {}
            for cls_idx, cls_name in enumerate(class_map):
                cls_name = f'{cls_idx:02d}-{cls_name}'
                if cls_idx in cls_metrics:
                    ap, p, r, f1 = cls_metrics[cls_idx]
                else:
                    ap, p, r, f1 = 0, 0, 0, 0
                aps[cls_name] = ap
                ps[cls_name] = p
                rs[cls_name] = r
                f1s[cls_name] = f1
            writer.writer.add_scalars('metrics/AP', aps, global_step=epoch)
            writer.writer.add_scalars('metrics/Precision', ps, global_step=epoch)
            writer.writer.add_scalars('metrics/Recall', rs, global_step=epoch)
            writer.writer.add_scalars('metrics/F1', f1s, global_step=epoch)
            writer.writer.add_scalar('metrics/mAP', mAP, global_step=epoch)
        writer.writer.add_scalars('loss/all', {'train': train_m['loss']},
                                  global_step=epoch)
        writer.writer.add_scalars('loss/loc', {'train': train_m['loc']},
                                  global_step=epoch)
        writer.writer.add_scalars('loss/conf', {'train': train_m['conf']},
                                  global_step=epoch)
        writer.writer.add_scalars('loss/cls', {'train': train_m['cls']},
                                  global_step=epoch)
        writer.writer.add_scalars('metrics/obj', {'train': train_m['obj']},
                                  global_step=epoch)
        writer.writer.add_scalars('metrics/noobj', {'train': train_m['noobj']},
                                  global_step=epoch)
        writer.writer.add_scalars('metrics/acc', {'train': train_m['cls_acc']},
                                  global_step=epoch)
        writer.writer.flush()

    @trainer.on(Events.ITERATION_COMPLETED)
    def plot_lr_per_iter(engine: Engine):
        iteration = engine.state.iteration
        lr = optimizer.param_groups[0]['lr']
        writer.writer.add_scalar('status/lr', lr, global_step=iteration)

    # draw graph
    # with torch.no_grad():
    #     ipt = torch.rand(1, 3, cfg['train']['img_h'], cfg['train']['img_w']).to(device=device)
    #     writer.writer.add_graph(model, ipt)
    return trainer, evaluator


def init_dist_if_possible(dist, backend, init_method, world_size, rank):
    if dist:
        # init_method = None if backend == 'nccl' else init_method
        distributed.init_process_group(backend=backend, init_method=init_method,
                                       world_size=world_size, rank=rank, group_name='SyncMe')
        # torch.cuda.set_device(rank)
        print(f'Distribution training status: {"success" if distributed.is_initialized() else "failed"}')


# @begin.start(auto_convert=True, env_prefix='YOLO_')
def run(cfg, name,
        dist, backend, init_method, world_size, rank=0,
        level='INFO', device=''):
    init_dist_if_possible(dist, backend, init_method, world_size, rank)
    cfg = prepare_cfg(cfg, name, level)

    device = select_device(device)
    model, optimizer, loss_fn = make_model_optimizer_loss_fn(cfg)
    train_dl, val_dl = get_dataloader(cfg)
    logging.info(f'train dataset: {len(train_dl.dataset)} images; validation {len(val_dl.dataset)} images')

    model, optimizer, yolo_loss_fn = wrap_model_optimizer(model, optimizer, loss_fn, cfg, device)
    trainer, evaluator = create_trainer_evaluator(cfg, model, optimizer, yolo_loss_fn, train_dl, val_dl,
                                                  device)
    # make a summary
    summary(model, (3, cfg['yolo']['im_size'], cfg['yolo']['im_size']), device=device.type)
    trainer.run(train_dl, cfg['train']['epochs'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--cfg', type=str, required=True, help='config path (Required)')
    parser.add_argument('--name', type=str, default='unnamed', help='experiment name, (Default: unnamed)')
    parser.add_argument('--dist', action='store_true', required=False, help='Use dist training (Default: False)')
    parser.add_argument('--backend', type=str, default='nccl', required=False, help='backend for dist training')
    parser.add_argument('--init_method', type=str, default='tcp://127.0.0.1:6666', required=False)
    parser.add_argument('--world_size', type=int, default=1, required=False)
    parser.add_argument('--local_rank', type=int, default=0, required=False)
    parser.add_argument('--level', type=str, choices=['notset', 'debug', 'info', 'warn', 'error', 'fatal'],
                        default='warn')
    parser.add_argument('--seed', type=int, required=False)
    parser.add_argument('--device', type=str, default='')
    opt = parser.parse_args()
    if opt.seed is not None:
        import random

        seed = opt.seed
        random.seed(seed)
        torch.manual_seed(seed)
        if torch.cuda.is_available():
            import torch.backends.cudnn as cudnn

            cudnn.deterministic = True
    run(opt.cfg, opt.name, opt.dist, opt.backend, opt.init_method, opt.world_size, opt.local_rank, opt.level,
        opt.device)
