import argparse

import numpy as np
import toml
import tqdm
import yaml
from torch.utils.data import ConcatDataset

# noinspection PyUnresolvedReferences
from utils.datasets import ListDataset, PascalVOC
from utils.snippets import cluster_anchors, load_classes

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--root', type=str, required=True, help='root folder')
    parser.add_argument('--nums', type=int, default=9, help='anchors numbers')
    parser.add_argument('--img_size', type=int, default=416, help='resize image')
    opt = parser.parse_args()
    class_maps, _ = load_classes(f'{opt.root}/VOCdevkit/classes.names')
    voc = ConcatDataset([
        PascalVOC(root=opt.root, class_map=class_maps, img_size=opt.img_size, year='2012', image_set='trainval',
                  augments=None),
        PascalVOC(root=opt.root, class_map=class_maps, img_size=opt.img_size, year='2007', image_set='trainval',
                  augments=None)
        # ListDataset('/home/troy/Projects/Datasets/anime-face/train.txt', img_size=opt.img_size, train=True),
        # ListDataset('/home/troy/Projects/Datasets/anime-face/valid.txt', img_size=opt.img_size, train=False),
    ])
    anchors = []
    # noinspection DuplicatedCode
    for img, boxes, _ in tqdm.tqdm(voc, leave=False):
        anchors.append(boxes[:, 4:])
    anchors = np.vstack(anchors)
    cores, dist = cluster_anchors(anchors, opt.nums, savefig=True)
    with open('anchors.toml', 'w') as f:
        toml.dump({'anchors': cores}, f)
    print(cores)
    print(f'distance = {dist}')
