import logging

import begin
import torch
from ignite.contrib.handlers import ProgressBar
from ignite.engine import Engine

from models.loss import YOLOLoss
from models.net import YOLONet
from utils.ignite_extends import mAPMetric
from utils.snippets import load_classes
from utils.train_utils import prepare_cfg, get_dataloader, get_evaluator_fn


@begin.start(auto_convert=True, env_prefix='YOLO_')
def eval(cfg,
         weights,
         name='unnamed',
         conf=0.5,
         iou=0.5,
         nms=0.45,
         top_k=25,
         export_csv=False):
    device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')
    cfg = prepare_cfg(cfg, name, level='INFO', train=False)
    class_map, _ = load_classes(cfg['dataset']['class_path'])
    _, val_dl = get_dataloader(cfg)
    model: torch.nn.Module = YOLONet(cfg['yolo'])
    model.to(device=device)
    with open(weights, 'rb') as fp:
        states_dict = torch.load(fp, map_location=device)
    model.load_state_dict(states_dict, strict=True)
    logging.info(f'Load weights: {weights}')

    evaluator = Engine(get_evaluator_fn(cfg, model, loss_fn=YOLOLoss(cfg['yolo']), device=device, non_blocking=True))
    metric = mAPMetric(
        iou_ignore=iou,
        conf_threshold=conf,
        nms_threshold=nms,
        top_k=top_k
    )
    pbar = ProgressBar()
    metric.attach(evaluator, 'ap')
    pbar.attach(evaluator, metric_names=['loss'])
    aps = evaluator.run(val_dl).metrics['ap']
    if export_csv:
        import pandas as pd
        df = pd.DataFrame(aps[0], columns=['name', 'ap', 'precision', 'recall', 'f1'])
        df['name'] = df['name'].map(lambda x: class_map[int(x)])
        df.to_csv(f'{name}-{cfg["try"]}.csv', index=False, float_format='%.4f')
    for clz, ap, p, r, f1 in aps[0]:
        print(f'{class_map[int(clz)]}:\t{ap:.3f}')
    print(f'mAP: {aps[1]}')
