import numpy as np
import tqdm
from torch.utils.data import ConcatDataset
from torchvision.datasets import VOCDetection

from utils.snippets import load_classes

train_voc = ConcatDataset([
    VOCDetection('/home/troy/Projects/Datasets/official', '2012', 'trainval'),
    VOCDetection('/home/troy/Projects/Datasets/official', '2007', 'train')
])
val_voc = VOCDetection('/home/troy/Projects/Datasets/official', '2007', 'val')
class_map, _ = load_classes('/home/troy/Projects/Datasets/official/VOCdevkit/classes.names')
class_map = {n: i for (i, n) in enumerate(class_map)}


def convert_anno_to_bbox(anno, keep_difficult=False):
    anno = anno['annotation']
    objs = anno['object']
    bbs = []
    if isinstance(objs, dict):
        b = objs['bndbox']
        l = float(class_map[objs['name'].lower()])
        bbs.append([l, *[int(c) for c in [b['xmin'], b['ymin'], b['xmax'], b['ymax']]]])
    else:
        for b in objs:
            l = float(class_map[b['name'].lower()])
            b = b['bndbox']
            bbs.append([l, *[int(c) for c in [b['xmin'], b['ymin'], b['xmax'], b['ymax']]]])
    bbs = np.array(bbs)
    return bbs


def save_voc(dataset, gather_path, img_out, label_out, idx):
    img_list = []
    for img, anno in tqdm.tqdm(dataset):
        bbs = convert_anno_to_bbox(anno)
        img_p = f'{img_out}/f{idx:07d}.jpg'
        img.save(img_p, 'jpeg')
        np.savetxt(f'{label_out}/f{idx:07d}.txt', bbs, delimiter=' ')
        img_list.append(img_p)
        idx += 1
    with open(gather_path, 'w') as fp:
        fp.write('\n'.join(img_list))
    return idx


if __name__ == '__main__':
    import os

    out = '/home/troy/Projects/Datasets/VOCList'
    TRAIN_list = f'{out}/train.txt'
    VAL_list = f'{out}/valid.txt'
    IMG_out = f'{out}/images'
    LABEL_out = f'{out}/labels'
    os.makedirs(IMG_out, exist_ok=True)
    os.makedirs(LABEL_out, exist_ok=True)
    idx = save_voc(train_voc, TRAIN_list, IMG_out, LABEL_out, 0)
    idx = save_voc(val_voc, VAL_list, IMG_out, LABEL_out, idx)
