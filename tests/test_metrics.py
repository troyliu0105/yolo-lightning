def test_mAP():
    import numpy as np
    import torch
    from utils.metrics import compute_ap_map
    gt = np.loadtxt('../data/gt.txt').astype(np.float32)
    raw_pred = np.loadtxt('../data/pred.txt').astype(np.float32)
    gt[:, 4] += gt[:, 2]
    gt[:, 5] += gt[:, 3]
    raw_pred[:, 5] += raw_pred[:, 3]
    raw_pred[:, 6] += raw_pred[:, 4]

    preds = np.empty((raw_pred.shape[0], 8), dtype=raw_pred.dtype)
    preds[:, 0] = raw_pred[:, 0]
    preds[:, 1:5] = raw_pred[:, 3:]
    preds[:, 5] = raw_pred[:, 2]
    preds[:, 6] = 1
    preds[:, 7] = 0
    n = np.unique(preds[:, 0])
    preds = [torch.from_numpy(preds[preds[:, 0] == i, 1:]) for i in n]
    gt = torch.from_numpy(gt)
    # [unique_classes, ap, p, r, f1]
    out = compute_ap_map(preds, gt, iou_threshold=0.3, from_xyxy=True)
    mAP = out[1]
    assert round(mAP, ndigits=4) == 0.2457, 'wrong mAP'
