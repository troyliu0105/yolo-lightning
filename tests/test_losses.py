import torch


def test_giou_loss():
    from models.loss import generalized_iou_loss
    box1 = torch.tensor([[100, 100, 250, 250]])
    box2 = torch.tensor([[150, 150, 300, 300]])
    loss = generalized_iou_loss(box1, box2)
    assert 0.8393 == round(loss.item(), ndigits=4), 'loss is not equal...'


def test_focal_loss():
    from models.loss import FocalLoss
    focalloss = FocalLoss(torch.nn.BCELoss(), gamma=2, alpha=0.25)
    y_hat = torch.tensor([[0.5, 0.7, 0.1]])
    y = torch.tensor([[0., 1., 1.]])
    loss = focalloss(y_hat, y)
    assert round(loss.item(), ndigits=4) == 0.1725, 'loss is not equal...'
    y_hat[0, 2] = 0.9
    loss2 = focalloss(y_hat, y)
    assert loss.item() > loss2.item(), 'second loss must be smaller than first one'
